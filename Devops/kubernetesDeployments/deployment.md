****## KUBERNETES

## ->Quick run container
kubectl run hazelcast --image=hazelcast --port=5701

#### Deploy
kubectl apply -f deployment.yaml

-> Define Service - To expose pods to outside 
kubectl expose deployment roomsapp-deployment --type=NodePort

minikube service roomsapp-deployment --url
->GET
curl $(minikube service roomsapp-deployment --url)/revision

kubectl get pod
->

kubectl get pods [pod name]

kubectl describe pod [pod name]

kubectl port-forward roomsapp-deployment-69499ff655-prchk 5555:6666

kubectl attach roomsapp-deployment-69499ff655-prchk -c roomsapp

kubectl exec -it roomsapp-deployment-69499ff655-prchk bash

kubectl label pods roomsapp-deployment-69499ff655-prchk healthy=false

kubectl describe pod roomsapp-deployment-69499ff655-prchk

kubectl scale --replicas=3 deployment roomsapp-deployment

kubectl get deployments

kubectl get deployments roomsapp-dep loyment

kubectl describe deployments roomsapp-deployment

kubectl expose deployment roomsapp-deployment --type=LoadBalancer --port=5555 --target-port=5555 --name=roomsapp-load-balancer

kubectl describe services roomsapp-load-balancer

kubectl get deployments

kubectl rollout status deployment roomsapp-deployment

kubectl set image deployment/roomsapp-deployment roomsapp=antoniocanelas/roomsapp:102

kubectl  rollout history deployment/roomsapp-deployment

kubectl  rollout history deployment/roomsapp-deployment --revision=2

curl $(minikube service roomsapp-deployment --url)/revision

kubectl label node minikube storageType=ssd

->check labeled storageType=ssd

kubectl describe node minikube

kubectl apply -f Devops/deployment_ssd.yaml  

kubectl apply -f Devops/deployment_health_check.yaml

kubectl apply -f Devops/deployment_health_check.yaml

kubectl apply -f Devops/deployment_health_check.yaml

->One time to configure UI in kubernetes
kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml

kubectl proxy

->Browser
http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/.
