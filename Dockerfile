FROM antoniocanelas/jdk-mvn-git

ARG buildNumb

RUN git clone https://antoniocanelas@bitbucket.org/antoniocanelas/rooms-manager.git

WORKDIR /rooms-manager

RUN mvn clean install && (echo Commit: && git rev-list HEAD --count && git log --oneline -1 && echo Build: && echo ${buildNumb} ) > revision.txt

EXPOSE 5555

CMD ["mvn", "spring-boot:run", "-Ptest"]