package rooms.manager.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import rooms.manager.UnitaryTest;
import rooms.manager.model.DTO.RoomConverter;
import rooms.manager.model.DTO.RoomDTO;
import rooms.manager.model.Room;
import rooms.manager.repository.RoomDAOTestRepository;
import rooms.manager.service.imp.roomServiceImp.RoomServiceImp;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@UnitaryTest
class RoomControllerTest {
    private RoomConverter roomConverter = new RoomConverter();


    private RoomDTO roomDTOExpected;
    private RoomDTO roomDTOTest;
    private RoomDTO roomDTOResponse;
    private RoomController roomController;
    private RoomDAOTestRepository roomDAOTestRepository;

    @BeforeEach
    void initAll() {
        roomDAOTestRepository = new RoomDAOTestRepository();
        RoomServiceImp roomServiceImp = new RoomServiceImp(roomDAOTestRepository);
        roomController = new RoomController(roomServiceImp);
    }

    @Test
    void addroom() {
        //GIVEN
        roomDTOExpected = new RoomDTO("add room test", 1, Room.RoomType.SINGLE,250);
        //WHEN
        roomDTOResponse = roomController.addroom(roomDTOExpected);
        roomDTOTest = roomConverter.convertFromEntity(roomDAOTestRepository.findById(0).get());
        roomDTOExpected.setRoom_Id(0);
        //THEN
        assertEquals(roomDTOExpected, roomDTOTest);
        assertEquals(roomDTOExpected, roomDTOResponse);
    }

    @Test
    void updateroom() {
        //GIVEN
        roomDAOTestRepository.save(new Room("add room test", 1, Room.RoomType.SINGLE,250));
        roomDTOExpected = new RoomDTO("add room test - UPDATED", 1, Room.RoomType.SINGLE,250);
        roomDTOExpected.setRoom_Id(0);
        //WHEN
        roomDTOResponse = roomController.updateroom(roomDTOExpected);
        roomDTOTest = roomConverter.convertFromEntity(roomDAOTestRepository.findById(0).get());
        //THEN
        assertEquals(roomDTOExpected, roomDTOTest);
        assertEquals(roomDTOExpected, roomDTOResponse);
    }

    @Test
    void getroom() {
        //GIVEN
        roomDTOExpected = new RoomDTO("get room test", 1, Room.RoomType.SINGLE,250);
        roomDTOExpected.setRoom_Id(0);
        roomDAOTestRepository.save(new Room("get room test", 1, Room.RoomType.SINGLE,250));
        //WHEN
        roomDTOResponse = roomController.getroom(0);
        //THEN
        assertEquals(roomDTOExpected, roomDTOResponse);
    }

    @Test
    void deleteAllrooms() {
        //GIVEN
        roomDAOTestRepository.save(new Room("delete room1 test", 1, Room.RoomType.SINGLE,250));
        roomDAOTestRepository.save(new Room("delete room2 test", 1, Room.RoomType.SINGLE,250));
        assertTrue(roomDAOTestRepository.existsByName("delete room1 test"));
        assertTrue(roomDAOTestRepository.existsByName("delete room2 test"));
        //WHEN
        roomController.deleteAllrooms();
        //THEN
        assertFalse(roomDAOTestRepository.existsByName("delete room1 test"));
        assertFalse(roomDAOTestRepository.existsByName("delete room2 test"));
    }

    @Test
    void deleteroom() {
        //GIVEN
        roomDAOTestRepository.save(new Room("delete room1 test", 1, Room.RoomType.SINGLE,250));
        roomDAOTestRepository.save(new Room("delete room2 test", 1, Room.RoomType.SINGLE,250));
        assertTrue(roomDAOTestRepository.existsByName("delete room1 test"));
        assertTrue(roomDAOTestRepository.existsByName("delete room2 test"));
        //WHEN
        roomController.deleteroom(0);
        //THEN
        assertFalse(roomDAOTestRepository.existsByName("delete room1 test"));
        assertTrue(roomDAOTestRepository.existsByName("delete room2 test"));
    }

    @Test
    void getAllRooms() {
        //GIVEN
        roomDAOTestRepository.save(new Room("getAllRooms room1 test", 1, Room.RoomType.SINGLE,250));
        roomDAOTestRepository.save(new Room("getAllRooms room2 test", 1, Room.RoomType.SINGLE,250));
        assertTrue(roomDAOTestRepository.existsByName("getAllRooms room1 test"));
        assertTrue(roomDAOTestRepository.existsByName("getAllRooms room2 test"));
        //WHEN
        List<RoomDTO> roomDTOList = roomController.getAllRooms();
        //THEN
        assertEquals(2, roomDTOList.size());
        assertTrue(roomDAOTestRepository.existsByName("getAllRooms room1 test"));
        assertTrue(roomDAOTestRepository.existsByName("getAllRooms room2 test"));
    }
}