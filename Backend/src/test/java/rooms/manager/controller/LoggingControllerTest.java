package rooms.manager.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import rooms.manager.UnitaryTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@UnitaryTest
class LoggingControllerTest {
    String textResult;
    String textExpected;
    LoggingController loggingController;

    @BeforeEach
    void setUp() {
        loggingController = new LoggingController();
    }

    @Test
    void index() {
        //GIVEN
        textExpected = "Check out the Logs to see the output... :-) - Commit 106";
        //WHEN
        textResult = loggingController.index();
        //THEN
        assertEquals(textExpected, textResult);
    }

}