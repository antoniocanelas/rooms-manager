package rooms.manager.controller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import rooms.manager.SystemTest;
import rooms.manager.config.JwtTokenUtil;
import rooms.manager.model.Constants;
import rooms.manager.model.DTO.RoomConverter;
import rooms.manager.model.DTO.RoomDTO;
import rooms.manager.model.Room;
import rooms.manager.model.User;
import rooms.manager.repository.RoomDAO;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@SystemTest
class RoomControllerSYSTest {
    private RoomConverter roomConverter = new RoomConverter();


    private static HttpHeaders guestHeaders;
    private static HttpHeaders admminHeaders;
    private RoomDTO roomDTOExpected;
    private ResponseEntity<RoomDTO> response;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private RoomDAO roomDAO;

    @BeforeAll
    static void setUp() {
        User guestUser = new User();
        User adminUser = new User();
        JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();

        guestUser.setUsername("Guest");
        guestUser.setPassword("password");
        String guestToken = jwtTokenUtil.generateToken(guestUser);
        guestHeaders = new HttpHeaders();
        guestHeaders.setContentType(MediaType.APPLICATION_JSON);
        guestHeaders.set(Constants.HEADER_STRING, Constants.TOKEN_PREFIX + guestToken);

        adminUser.setUsername("Admin");
        adminUser.setPassword("password");
        String adminToken = jwtTokenUtil.generateToken(adminUser);
        admminHeaders = new HttpHeaders();
        admminHeaders.setContentType(MediaType.APPLICATION_JSON);
        admminHeaders.set(Constants.HEADER_STRING, Constants.TOKEN_PREFIX + adminToken);

    }

    @Test
    void getRoom() {
        //GIVEN
        Room room = roomDAO.save(new Room("getRoom", 111, Room.RoomType.SINGLE,250));
        HttpEntity<RoomDTO> entity = new HttpEntity<>(roomDTOExpected, guestHeaders);
        assertTrue(roomDAO.existsByName(("getRoom")));
        roomDTOExpected = roomConverter.convertFromEntity(roomDAO.findById(1).get());
        //WHEN
        response = testRestTemplate.exchange("/room/" + roomDTOExpected.getRoom_Id(), HttpMethod.GET, entity, RoomDTO.class);
        //THEN
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(roomDTOExpected, response.getBody());
    }

    @Test
    void getAllRooms() {
        //GIVEN
        HttpEntity<RoomDTO> entity = new HttpEntity<>(roomDTOExpected, admminHeaders);
        assertTrue(roomDAO.existsByName(("getRoom")));
        assertTrue(roomDAO.existsByName(("getAllRooms")));
        //WHEN
        ResponseEntity<List<RoomDTO>> responseList;
        responseList = testRestTemplate.exchange("/room/all", HttpMethod.GET, entity, new ParameterizedTypeReference<List<RoomDTO>>() {
        });
        //THEN
        assertEquals(HttpStatus.OK, responseList.getStatusCode());
        assertTrue(responseList.getBody().contains(roomConverter.convertFromEntity(roomDAO.findById(1).get())));
        assertTrue(responseList.getBody().contains(roomConverter.convertFromEntity(roomDAO.findById(2).get())));
    }

    @Test
    void addRoom() {
        //GIVEN
        roomDTOExpected = new RoomDTO("add room test", 1, Room.RoomType.SINGLE,250);

        HttpEntity<RoomDTO> entity = new HttpEntity<>(roomDTOExpected, admminHeaders);
        //WHEN

        response = testRestTemplate.exchange("/room/addroom", HttpMethod.POST, entity, RoomDTO.class);
        //THEN
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    void updateRoom() {
        //GIVEN
        roomDTOExpected = new RoomDTO(0, "update room test", 1, Room.RoomType.SINGLE,250);
        HttpEntity<RoomDTO> entity = new HttpEntity<>(roomDTOExpected, admminHeaders);
        //When

        response = testRestTemplate.exchange("/room/updateroom", HttpMethod.PUT, entity, RoomDTO.class);
        //THEN
        assertEquals(HttpStatus.OK, response.getStatusCode());

    }

    @Test
    void deleteRoom() {
        //GIVEN
        Optional<Room> roomDTO;
        Room room = roomDAO.save(new Room("deleteroom", 999, Room.RoomType.SINGLE,250));
        assertTrue(roomDAO.existsById((room.getRoom_Id())));
        HttpEntity<String> entity = new HttpEntity<>("", guestHeaders);
        //When
        ResponseEntity<RoomDTO> response;
        response = testRestTemplate.exchange("/room/" + room.getRoom_Id(), HttpMethod.DELETE, entity, RoomDTO.class);
        //THEN
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @DirtiesContext
    void deleteAllRooms() {
        //GIVEN
        assertFalse(roomDAO.findAll().isEmpty());
        HttpEntity<String> entity = new HttpEntity<>("", admminHeaders);
        //When
        ResponseEntity<RoomDTO> response;
        response = testRestTemplate.exchange("/room/all", HttpMethod.DELETE, entity, RoomDTO.class);
        //THEN
        assertTrue(roomDAO.findAll().isEmpty());
        assertEquals(HttpStatus.OK, response.getStatusCode());

    }
}