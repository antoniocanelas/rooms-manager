package rooms.manager.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.ResourceAccessException;
import rooms.manager.SystemTest;
import rooms.manager.config.JwtTokenUtil;
import rooms.manager.model.DTO.LoginUserDTO;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@SystemTest
class AuthenticationControllerSYSTest {
    @Autowired
    TestRestTemplate restTemplate;
    private ResponseEntity<JwtTokenUtil> responseEntity;

    @Test
    void loginSuccess() {        //GIVEN
        LoginUserDTO loginUserDTO = new LoginUserDTO();
        loginUserDTO.setUsername("Admin");
        loginUserDTO.setPassword("password");
        //When
        responseEntity = restTemplate.postForEntity("/token/generate-token", loginUserDTO, JwtTokenUtil.class);
        //THEN
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    void loginFailWrongPassword() {
        //GIVEN
        LoginUserDTO loginUserDTO = new LoginUserDTO();
        loginUserDTO.setUsername("Admin");
        loginUserDTO.setPassword("wrongPassword");
        //WHEN
        Throwable exception = assertThrows(ResourceAccessException.class, () -> responseEntity = restTemplate.postForEntity("/token/generate-token", loginUserDTO, JwtTokenUtil.class));
        //THEN
        assertTrue(exception.getMessage().contains("I/O error on POST request for"));
    }

    @Test
    void loginFailWrongUser() {
        //GIVEN
        LoginUserDTO loginUserDTO = new LoginUserDTO();
        loginUserDTO.setUsername("wrongUser");
        loginUserDTO.setPassword("password");
        //WHEN
        Throwable exception = assertThrows(ResourceAccessException.class, () -> responseEntity = restTemplate.postForEntity("/token/generate-token", loginUserDTO, JwtTokenUtil.class));
        //THEN
        assertTrue(exception.getMessage().contains("I/O error on POST request for"));
    }

    @Test
    void loginFailNullUser() {
        //GIVEN
        LoginUserDTO loginUserDTO = new LoginUserDTO();
        loginUserDTO.setUsername(null);
        loginUserDTO.setPassword("password");
        //WHEN
        Throwable exception = assertThrows(ResourceAccessException.class, () -> responseEntity = restTemplate.postForEntity("/token/generate-token", loginUserDTO, JwtTokenUtil.class));
        //THEN
        assertTrue(exception.getMessage().contains("I/O error on POST request for"));
    }

    @Test
    void loginFailNullPassword() {
        //GIVEN
        LoginUserDTO loginUserDTO = new LoginUserDTO();
        loginUserDTO.setUsername("Admin");
        loginUserDTO.setPassword(null);
        //WHEN
        Throwable exception = assertThrows(ResourceAccessException.class, () -> responseEntity = restTemplate.postForEntity("/token/generate-token", loginUserDTO, JwtTokenUtil.class));
        //THEN
        assertTrue(exception.getMessage().contains("I/O error on POST request for"));
    }

}