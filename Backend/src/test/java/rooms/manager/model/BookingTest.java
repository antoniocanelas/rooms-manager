package rooms.manager.model;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import rooms.manager.UnitaryTest;
import rooms.manager.model.Cost.CostStrategyImp.DailyContractStrategy;
import rooms.manager.model.Cost.CostStrategyImp.YearlyContractStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@ActiveProfiles("test")
@UnitaryTest
class BookingTest {

    @Test
    void getCostStrategy() {
        //GIVEN
        YearlyContractStrategy yearlyContractStrategy = new YearlyContractStrategy();
        //WHEN
        Booking booking = new Booking(yearlyContractStrategy);
        //THEN
        assertEquals(yearlyContractStrategy, booking.getCostStrategy());
    }

    @Test
    void setCostStrategy() {
        //GIVEN
        YearlyContractStrategy yearlyContractStrategy = new YearlyContractStrategy();
        Booking booking = new Booking(new DailyContractStrategy());
        assertNotEquals(yearlyContractStrategy, booking.getCostStrategy());
        //WHEN
        booking.setCostStrategy(yearlyContractStrategy);
        //THEN
        assertEquals(yearlyContractStrategy, booking.getCostStrategy());
    }

    @Test
    void SetGetBooking_Id() {
        //GIVEN
        Booking booking = new Booking(new DailyContractStrategy());
        assertNotEquals(1, booking.getBooking_Id());
        //WHEN
        booking.setBooking_Id(1);
        //THEN
        assertEquals(1, booking.getBooking_Id());
    }
}