package rooms.manager.model.Cost.CostStrategyImp;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import rooms.manager.UnitaryTest;
import rooms.manager.model.Cost.BookingCost;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@UnitaryTest
class DailyContractStrategyTest {


    @Test
    void discountRate() {
        //GIVEN
        BookingCost bookingCost;
        //WHEN
        bookingCost= new BookingCost(new DailyContractStrategy());
        //THEN
        assertEquals(1, bookingCost.valueDiscountRate());
    }

}