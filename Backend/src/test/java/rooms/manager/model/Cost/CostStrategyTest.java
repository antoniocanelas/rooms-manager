package rooms.manager.model.Cost;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.context.ActiveProfiles;
import rooms.manager.UnitaryTest;
import rooms.manager.model.Cost.CostStrategyImp.DailyContractStrategy;
import rooms.manager.model.Cost.CostStrategyImp.MonthlyContractStrategy;
import rooms.manager.model.Cost.CostStrategyImp.YearlyContractStrategy;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@UnitaryTest
public class CostStrategyTest {
    static Collection<Object[]> dataProvider() {
        return Arrays.asList(
                new Object[]{
                        new DailyContractStrategy(), 1.0},
                new Object[]{
                        new MonthlyContractStrategy(), 0.9},
                new Object[]{
                        new YearlyContractStrategy(), 0.8}
        );
    }

    /**
     * Test if executing the strategy gives the correct response
     */
    @ParameterizedTest
    @MethodSource("dataProvider")
    public void testExecute(CostStrategy strategy, Double expectedResult) {
        assertEquals(expectedResult, (Double) strategy.discountRate());
    }


}