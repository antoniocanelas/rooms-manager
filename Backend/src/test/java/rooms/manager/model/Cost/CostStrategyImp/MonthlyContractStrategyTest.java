package rooms.manager.model.Cost.CostStrategyImp;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import rooms.manager.UnitaryTest;
import rooms.manager.model.Cost.BookingCost;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@UnitaryTest
class MonthlyContractStrategyTest {

    @Test
    void discountRate() {
        //GIVEN
        //WHEN
        //THEN

        BookingCost bookingCost = new BookingCost(new MonthlyContractStrategy());
        assertEquals(0.9, bookingCost.valueDiscountRate());
    }
}