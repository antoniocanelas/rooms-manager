package rooms.manager.model.Cost;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import rooms.manager.UnitaryTest;
import rooms.manager.model.Cost.CostStrategyImp.DailyContractStrategy;

import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@UnitaryTest
class BookingCostTest {

    @Test
    void valueDiscountRate() {
        //GIVEN
        final DailyContractStrategy strategy = mock(DailyContractStrategy.class);
        final BookingCost bookingCost = new BookingCost(strategy);
        //WHEN
        bookingCost.valueDiscountRate();
        //THEN
        verify(strategy).discountRate();/*Verifies certain behavior <b>happened once</b>.*/
        verifyNoMoreInteractions(strategy);/*Checks if any of given mocks has any unverified interaction.*/
    }

    @Test
    void changeStrategy() {
        //GIVEN
        final DailyContractStrategy initialStrategy = mock(DailyContractStrategy.class);
        final BookingCost bookingCost = new BookingCost(initialStrategy);
        //WHEN
        bookingCost.valueDiscountRate();
        verify(initialStrategy).discountRate();
        final DailyContractStrategy newStrategy = mock(DailyContractStrategy.class);
        bookingCost.changeStrategy(newStrategy);
        bookingCost.valueDiscountRate();
        //THEN
        verify(newStrategy).discountRate();
        verifyNoMoreInteractions(newStrategy);
    }
}