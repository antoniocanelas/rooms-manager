package rooms.manager.service.adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BaseCheckTest {
    private String name;
    private int number;


    @BeforeEach
    void setUp() {
        name = "name";
        number = 1;

    }

    @Test
    public void testBaseCheck() {
        System.out.println("\nThe BaseCheck says...");
        ConcreteBaseCheck baseCheck = new ConcreteBaseCheck();
        baseCheck.requestDetails(name, number);
        baseCheck.requestName();
    }

    @Test
    public void testDiffCheck() {

        System.out.println("The DiffCheck says...");
        ConcreteDiffCheck diffCheck = new ConcreteDiffCheck();
        diffCheck.specificRequestDetails(number, name);
        diffCheck.specificRequestName();
    }

    @Test
    public void testTenantDocFactory() {
        ConcreteDiffCheck diffCheck = new ConcreteDiffCheck();
        System.out.println("\nThe DiffCheckAdapter says...");
        BaseCheck adapter = new DiffCheckAdapter(diffCheck);
        adapter.requestDetails(name, number);
        adapter.requestName();
    }
}