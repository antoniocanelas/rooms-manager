package rooms.manager.service.decorator;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import rooms.manager.UnitaryTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@UnitaryTest
class BaseRoomCostTest {

    double roomCostExpected, roomCostResult;
    String roomDescriptionExpected, roomDescriptionResult;
    BaseRoomCost baseRoomCost;

    @Test
    void singleRomWashoutAdditions() {
        //GIVEN
        roomCostExpected = 300;
        roomDescriptionExpected = "Single Room";

        //WHEN
        baseRoomCost = new SingleRoomCost();
        roomCostResult = baseRoomCost.cost();
        roomDescriptionResult = baseRoomCost.getDescription();

        //THEN
        assertEquals(roomCostExpected, roomCostResult);
        assertEquals(roomDescriptionExpected, roomDescriptionResult);
    }

    @Test
    void doubleRomWithAllAdditions() {
        //GIVEN
        roomCostExpected = 495;
        roomDescriptionExpected = "Double Room, with ironing service, with washing service, with room clean service";

        //WHEN
        baseRoomCost = new DoubleRoomCost();
        baseRoomCost = new LaundryIroningCost(baseRoomCost);
        baseRoomCost = new LaundryWashingCost(baseRoomCost);
        baseRoomCost = new RoomCleaningCost(baseRoomCost);

        roomCostResult = baseRoomCost.cost();
        roomDescriptionResult = baseRoomCost.getDescription();

        //THEN
        assertEquals(roomCostExpected, roomCostResult);
        assertEquals(roomDescriptionExpected, roomDescriptionResult);
    }

}