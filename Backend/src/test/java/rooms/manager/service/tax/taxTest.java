package rooms.manager.service.tax;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import rooms.manager.UnitaryTest;
import rooms.manager.model.Room;
import rooms.manager.service.tax.imp.TaxIS;
import rooms.manager.service.tax.imp.TaxLib;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@UnitaryTest
class TaxTest {
    @Test
    void getCostStrategy() {
        //GIVEN
        Room room = new Room(1,"room1", 1, Room.RoomType.SINGLE,200);
        Tax taxIS = new TaxIS();
        Tax taxLib = new TaxLib();
        //WHEN
        double taxISValue = taxIS.calculate(room);
        double taxLibValue = taxLib.calculate(room);
        //THEN
        assertEquals(20,taxISValue,.000001);
        assertEquals(56,taxLibValue,.000001);
    }

}