package rooms.manager.service.methodFactory;


import org.junit.jupiter.api.Test;
import rooms.manager.model.DTO.RoomDTO;
import rooms.manager.model.Room;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The Factory Method is a creational design pattern which uses factory methods to deal with the
 * problem of creating objects without specifying the exact class of object that will be created.
 * This is done by creating objects via calling a factory method either specified in an interface
 * and implemented by child classes, or implemented in a base class and optionally overridden by
 * derived classes—rather than by calling a constructor.
 *
 * <p>Factory produces the object of its liking.
 * The doc {@link Doc} manufactured by the
 * {@link DocFactory} depends on the kind of factory implementation it is referring to.
 * </p>
 */
class DocFactoryTest {

    Doc doc;

    /**
     * Testing {@link LandlordDocFactory} & {@link TenantDocFactory} to produce a correct output and asserting that the Doc is an instance
     * of {@link DocType}.
     */

    @Test
    public void testLandlordDocFactory() {
        // Landlord Document
        DocFactory docFactory = new LandlordDocFactory();

        assertEquals ("LandlordDoc{docType=Simplified Room Document, room_Id=10, name='SIMPLE', number=10, type='SINGLE', monthRent=300.0}",createSimpleDoc(docFactory).toString());
        assertEquals ("LandlordDoc{docType=Detailed Room Document, room_Id=10, name='DETAILED', number=10, type='DOUBLE', monthRent=500.0}",createDetailedDoc(docFactory).toString());


    }

    @Test
    public void testTenantDocFactory() {
        // Landlord Document
        DocFactory docFactory = new TenantDocFactory();

        assertEquals ("TenantDoc{docType=Simplified Room Document, name='SIMPLE', type='SINGLE', monthRent=300.0}",createSimpleDoc(docFactory).toString());
        assertEquals ("TenantDoc{docType=Detailed Room Document, name='DETAILED', type='DOUBLE', monthRent=500.0}",createDetailedDoc(docFactory).toString());


    }

    private Doc createSimpleDoc(DocFactory docFactory) {
        doc = docFactory.createDoc(DocType.SIMPLE, new RoomDTO(10, "SIMPLE", 10, Room.RoomType.SINGLE, 300));
        return doc;

    }

    private Doc createDetailedDoc(DocFactory docFactory) {

        doc = docFactory.createDoc(DocType.DETAILED, new RoomDTO(10, "DETAILED", 10, Room.RoomType.DOUBLE, 500));
        return doc;

    }

}
