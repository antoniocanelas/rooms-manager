package rooms.manager.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import rooms.manager.IntegrationTest;
import rooms.manager.WithMockUserRoleAdminCanelas;
import rooms.manager.model.DTO.UserDTO;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
@IntegrationTest
class UserServiceTest {

    @Qualifier("userService")
    @Autowired
    private UserService userService;

    @Test
    void save() {
    }

    @Test
    void delete() {
    }

    @Test
    @WithMockUserRoleAdminCanelas
    void findAllUsingMetaAnnotations() {

        List<UserDTO> userDTOList = userService.findAll();

        assertEquals(4, userDTOList.size());
    }

    @Test
    @WithMockUser(username = "antoniocanelas", roles = {"ADMIN"})
    void findAllRoleAdminSuccess() {

        List<UserDTO> userDTOList = userService.findAll();
        assertEquals(4, userDTOList.size());
    }

    @Test
    @WithMockUser(username = "antoniocanelas", roles = {"GUEST"})
    void findAllRoleGuestNotAllowed() {
        Throwable exception = assertThrows(AccessDeniedException.class, () ->             userService.findAll());
//        assertEquals("Access is denied", exception.getMessage());
    }

    @Test
    @WithAnonymousUser
    void givenAnomynousUser_whenCallFindAll_thenAccessDenied() {
        Throwable exception = assertThrows(AccessDeniedException.class, () -> userService.findAll());
//        assertEquals("Access is denied", exception.getMessage());
    }

    @Test
    @WithMockUser(username = "antoniocanelas", roles = {"ADMIN"})
    void findByUsername() {
    }

    @Test
    @WithMockUser(username = "sysAdmin", authorities = {"SYS_ADMIN"})
    void findById() {
    }

    @Test
    void listUsernames() {
    }
}