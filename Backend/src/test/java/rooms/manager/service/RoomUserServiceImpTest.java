package rooms.manager.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import rooms.manager.UnitaryTest;
import rooms.manager.model.DTO.RoomConverter;
import rooms.manager.model.DTO.RoomDTO;
import rooms.manager.model.Room;
import rooms.manager.repository.RoomDAOTestRepository;
import rooms.manager.service.imp.roomServiceImp.RoomServiceImp;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@UnitaryTest
class RoomUserServiceImpTest {
    private RoomConverter roomConverter = new RoomConverter();


    private RoomDTO roomDTOExpected;
    private RoomDTO roomDTOTest;
    private RoomServiceImp roomServiceImp;
    private RoomDAOTestRepository roomDAOTestRepository;

    @BeforeEach
    void initAll() {
        roomDAOTestRepository = new RoomDAOTestRepository();
        roomServiceImp = new RoomServiceImp(roomDAOTestRepository);
    }

    @Test
    void addRoom() {
        //GIVEN
        roomDTOExpected = new RoomDTO("Room test", 1, Room.RoomType.SINGLE, 250);
        //WHEN
        roomDTOTest = roomServiceImp.addRoom(roomDTOExpected);
        roomDTOExpected.setRoom_Id(0);
        //THEN
        assertEquals(roomDTOExpected, roomDTOTest);
        assertEquals(roomDTOExpected.toString(), roomDTOTest.toString());
    }

    @Test
    void updateRoom() {
        //GIVEN
        RoomDTO roomDTOInit;
        roomDTOInit = new RoomDTO("Room test", 1, Room.RoomType.SINGLE,250);
        roomDTOTest = roomServiceImp.addRoom(roomDTOInit);
        roomDTOExpected = new RoomDTO(0, "Room test FINAL", 1, Room.RoomType.DOUBLE,250);
        //WHEN
        roomDTOTest = roomServiceImp.updateRoom(roomDTOExpected);
        assertEquals(roomDTOExpected.toString(), roomDTOTest.toString());
        assertEquals(roomConverter.convertFromEntity(roomDAOTestRepository.findById(0).get()).toString(), roomDTOExpected.toString());
        //THEN
        assertEquals(roomDTOExpected, roomDTOTest);
    }

    @Test
    void updateRoomFailNullId() {
        //GIVEN
        RoomDTO roomDTOInit;
        roomDTOInit = new RoomDTO("Room test", 1, Room.RoomType.SINGLE,250);
        roomDTOTest = roomServiceImp.addRoom(roomDTOInit);
        roomDTOExpected = new RoomDTO("Room test FINAL", 1, Room.RoomType.DOUBLE,250);
        //WHEN
        roomDTOTest = roomServiceImp.updateRoom(roomDTOExpected);
        //THEN
        assertNull(roomDTOTest);
    }

    @Test
    void deleteRoomById() {
        //GIVEN
        roomDAOTestRepository.save(new Room("Room test", 0, Room.RoomType.SINGLE,250));
        roomDAOTestRepository.save(new Room("Room deleted", 1, Room.RoomType.SINGLE,250));
        //WHEN
        roomServiceImp.deleteRoomById(1);
        //THEN
        assertTrue(roomDAOTestRepository.existsByName("Room test"));
        assertFalse(roomDAOTestRepository.existsByName("Room deleted"));

    }

    @Test
    void getRoomById() {
        //GIVEN
        roomDTOExpected = new RoomDTO(0, "Get Room test", 0, Room.RoomType.SINGLE,250);
        roomDAOTestRepository.save(new Room("Get Room test", 0, Room.RoomType.SINGLE,250));
        //WHEN
        roomDTOTest = roomServiceImp.getRoomById(0);
        //THEN
        assertEquals(roomDTOExpected, roomDTOTest);
    }

    @Test
    void deleteAllRooms() {
        //GIVEN
        roomDAOTestRepository.save(new Room("Room test1", 0, Room.RoomType.SINGLE,250));
        roomDAOTestRepository.save(new Room("Room test2", 0, Room.RoomType.SINGLE,250));
        roomDAOTestRepository.save(new Room("Room test3", 0, Room.RoomType.SINGLE,250));
        assertFalse(roomDAOTestRepository.findAll().isEmpty());
        //WHEN
        roomServiceImp.deleteAllRooms();
        //THEN
        assertTrue(roomDAOTestRepository.findAll().isEmpty());
    }

    @Test
    void getAllRooms() {
        //GIVEN
        roomDAOTestRepository.save(new Room("Get all Room test1", 0, Room.RoomType.SINGLE,250));
        roomDAOTestRepository.save(new Room("Get all Room test2", 0, Room.RoomType.SINGLE,250));
        roomDAOTestRepository.save(new Room("Get all Room test3", 0, Room.RoomType.SINGLE,250));
        List<RoomDTO> roomListExpected = new ArrayList<>();
        roomListExpected.add(new RoomDTO(0, "Get all Room test1", 0, Room.RoomType.SINGLE,250));
        roomListExpected.add(new RoomDTO(0, "Get all Room test2", 0, Room.RoomType.SINGLE,250));
        roomListExpected.add(new RoomDTO(0, "Get all Room test3", 0, Room.RoomType.SINGLE,250));
        //WHEN
        List<RoomDTO> roomListActual = roomServiceImp.getAllRooms();
        //THEN
        assertEquals(roomListExpected, roomListActual);
    }
}