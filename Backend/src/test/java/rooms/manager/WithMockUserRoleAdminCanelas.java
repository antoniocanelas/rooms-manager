package rooms.manager;

import org.springframework.security.test.context.support.WithMockUser;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@WithMockUser(username = "UserAdminCanelas", roles = {"ADMIN"})
public @interface WithMockUserRoleAdminCanelas {
}
