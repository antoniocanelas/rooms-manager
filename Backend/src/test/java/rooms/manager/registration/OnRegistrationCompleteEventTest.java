package rooms.manager.registration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import rooms.manager.UnitaryTest;
import rooms.manager.model.User;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@UnitaryTest
class OnRegistrationCompleteEventTest {
    OnRegistrationCompleteEvent onRegistrationCompleteEvent;
    User user;

    @BeforeEach
    void setUp() {
        user = new User();
         onRegistrationCompleteEvent = new OnRegistrationCompleteEvent(user, Locale.ENGLISH,"appUrl test");
    }

    @Test
    void getAppUrl() {
        assertEquals("appUrl test",onRegistrationCompleteEvent.getAppUrl());
    }

    @Test
    void getLocale() {
        assertEquals(Locale.ENGLISH,onRegistrationCompleteEvent.getLocale());
    }

    @Test
    void getUser() {
        assertEquals(user, onRegistrationCompleteEvent.getUser());
    }
}