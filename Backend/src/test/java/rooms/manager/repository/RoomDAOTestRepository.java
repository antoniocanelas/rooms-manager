package rooms.manager.repository;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import rooms.manager.UnitaryTest;
import rooms.manager.model.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class RoomDAOTestRepository implements RoomDAO {
    private static Integer id = 0;
    private final List<Room> roomList = new ArrayList<>();

    @Override
    public boolean existsByName(String name) {
        for (Room room : roomList) {
            if (room.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Room> findAll() {
        return roomList;
    }

    @Override
    public List<Room> findAll(Sort sort) {
        return null;
    }

    @Override
    public List<Room> findAllById(Iterable<Integer> integers) {
        return null;
    }

    @Override
    public <S extends Room> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Room> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Room> entities) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Room getOne(Integer integer) {
        return null;
    }

    @Override
    public <S extends Room> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Room> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public Page<Room> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Room> S save(S entity) {
        if (entity.getRoom_Id() == null) {
            entity.setRoom_Id(id);
            id = id + 1;
            roomList.add(entity);
        }
        roomList.remove(entity.getRoom_Id());
        roomList.add(entity);
        return entity;
    }

    @Override
    public Optional<Room> findById(Integer integer) {
        for (Room room : roomList) {
            if (room.getRoom_Id().equals(integer)) {
                return Optional.of(room);
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        for (Room room : roomList) {
            if (room.getRoom_Id().equals(integer)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {
                roomList.remove(integer.intValue());
    }

    @Override
    public void delete(Room entity) {

    }

    @Override
    public void deleteAll(Iterable<? extends Room> entities) {
    }

    @Override
    public void deleteAll() {
        roomList.clear();
    }

    @Override
    public <S extends Room> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Room> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Room> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Room> boolean exists(Example<S> example) {
        return false;
    }
}