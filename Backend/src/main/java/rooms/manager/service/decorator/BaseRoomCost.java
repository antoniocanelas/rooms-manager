package rooms.manager.service.decorator;

/**
 * Interface for Additional Costs
 */
public abstract class BaseRoomCost {
    String description = "Unknown Beverage";

    public String getDescription() {
        return description;
    }

    public abstract double cost();
}
