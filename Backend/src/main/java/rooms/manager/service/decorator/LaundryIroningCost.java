package rooms.manager.service.decorator;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LaundryIroningCost extends AdditionalDecorator {
    private final static Logger LOGGER = LoggerFactory.getLogger(LaundryIroningCost.class);
    BaseRoomCost baseRoomCost;


    public LaundryIroningCost(BaseRoomCost baseRoomCost) {
        this.baseRoomCost = baseRoomCost;
    }

    @Override
    public String getDescription() {
        LOGGER.info("LaundryIroningCost Decorator - old getDescription() ");
        return baseRoomCost.getDescription()+ ", with ironing service";
    }

    @Override
    public double cost() {
        LOGGER.info("LaundryIroningCost Decorator - old cost() ");
        return baseRoomCost.cost()+20;
    }
}
