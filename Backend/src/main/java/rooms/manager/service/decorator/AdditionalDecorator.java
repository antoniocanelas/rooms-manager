package rooms.manager.service.decorator;

public abstract class AdditionalDecorator extends BaseRoomCost{
    public abstract String getDescription();
}
