package rooms.manager.service.decorator;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DoubleRoomCost extends BaseRoomCost {

    public DoubleRoomCost() {
        description = "Double Room";
    }

    @Override
    public double cost() {
        return 450;
    }
}
