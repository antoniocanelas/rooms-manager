package rooms.manager.service.decorator;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


    public class RoomCleaningCost extends AdditionalDecorator {
        private final static Logger LOGGER = LoggerFactory.getLogger(rooms.manager.service.decorator.RoomCleaningCost.class);
        BaseRoomCost baseRoomCost;


        public RoomCleaningCost(BaseRoomCost baseRoomCost) {
            this.baseRoomCost = baseRoomCost;
        }

        @Override
        public String getDescription() {
            LOGGER.info("RoomCleaningCost Decorator - old getDescription() ");
            return baseRoomCost.getDescription() + ", with room clean service";
        }

        @Override
        public double cost() {
            LOGGER.info("RoomCleaningCost Decorator - old cost() ");
            return baseRoomCost.cost() + 15;
        }
    }