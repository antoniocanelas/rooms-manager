package rooms.manager.service.decorator;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SingleRoomCost extends BaseRoomCost {

    public SingleRoomCost() {
        description = "Single Room";
    }

    @Override
    public double cost() {
        return 300;
    }
}
