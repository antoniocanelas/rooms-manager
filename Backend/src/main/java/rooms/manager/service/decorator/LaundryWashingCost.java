package rooms.manager.service.decorator;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LaundryWashingCost extends AdditionalDecorator {
    private final static Logger LOGGER = LoggerFactory.getLogger(LaundryWashingCost.class);
    BaseRoomCost baseRoomCost;


    public LaundryWashingCost(BaseRoomCost baseRoomCost) {
        this.baseRoomCost = baseRoomCost;
    }

    @Override
    public String getDescription() {
        LOGGER.info("LaundryWashingCost Decorator - old getDescription() ");
        return baseRoomCost.getDescription() + ", with washing service";
    }

    @Override
    public double cost() {
        LOGGER.info("LaundryWashingCost Decorator - old cost() ");
        return baseRoomCost.cost() + 10;
    }
}