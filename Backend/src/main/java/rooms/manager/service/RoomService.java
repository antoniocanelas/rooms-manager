package rooms.manager.service;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import rooms.manager.model.DTO.DocDTO;
import rooms.manager.model.DTO.RoomDTO;

import java.util.List;

@Service
public interface RoomService {

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    RoomDTO addRoom(RoomDTO roomDTO);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    RoomDTO updateRoom(RoomDTO roomDTO);

    @PreAuthorize("hasRole('ROLE_GUEST')")
    RoomDTO getRoomById(int id);

    @PreAuthorize("hasRole('ROLE_GUEST')")
    void deleteRoomById(int id);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void deleteAllRooms();

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    List<RoomDTO> getAllRooms();

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_GUEST')")
    DocDTO getDocAbstractFactory(int id, String docTypeSelected);

    DocDTO getDocFactoryMethod(int id, String docTypeSelected);
}
