package rooms.manager.service.command;

import rooms.manager.model.Room;

public class RoomActivationCommand implements Command {
    Room room;

    public RoomActivationCommand(Room room) {
        this.room = room;
    }

    @Override
    public void execute() {
        room.activateRoom();
    }

    @Override
    public void unExecute() {
        room.inactivateRoom();
    }
}
