package rooms.manager.service.command;

/**
 *
 * Software Design Patterns - Command Pattern
 *
 **/

public interface Command {
    void execute();
    void unExecute();
}
