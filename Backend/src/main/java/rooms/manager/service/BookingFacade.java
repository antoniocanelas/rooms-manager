package rooms.manager.service;

import rooms.manager.model.Booking;
import rooms.manager.model.Room;
import rooms.manager.model.User;

/**
 * Software Design Patterns - Facade Pattern
 */
public class BookingFacade {
    User user;
    Room room;
    Booking booking;

    public BookingFacade(User user, Room room) {
        this.user = user;
        this.room = room;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public void buildBooking() {
        this.booking = new Booking();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public boolean isRoomActice() {
        return room.isActive();

    }

    public boolean isUserEnabled() {
        return user.isEnabled();
    }


    public boolean isRoomRegistered() {
        return !room.equals(null);
    }

    public boolean isUserRegistered() {
        return !user.equals(null);
    }

    public boolean roomsIsFreeInBookingDates() {
        return true;
    }

    public void bookingDefineUser() {
        booking.setUser(user);
    }

    public void bookingDefineRoom() {
        booking.setRoom(room);
    }

    public void bookingSetValid() {
        booking.setValid(true);
    }
}
