package rooms.manager.service;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import rooms.manager.model.DTO.UserRegistrationDTO;
import rooms.manager.model.User;
import rooms.manager.model.VerificationToken;

import java.util.List;

public interface SecurityService {

    UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException;

    User findByUsername(String username);

    User getUserToAuthentication(String username);

    void createVerificationTokenForUser(User user, String token);

    User registerNewUserAccount(UserRegistrationDTO userRegistrationDTO);

    boolean userActivation(String token);

    VerificationToken getVerificationToken(final String VerificationToken);

    List<SimpleGrantedAuthority> getAuthority();

}
