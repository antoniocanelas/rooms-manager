package rooms.manager.service.adapter;

public class BaseTestDrive {
    static String name = "NAME";
    static int number = 1;

    public static void main(String[] args) {
        System.out.println("\nThe BaseCheck says...");
        ConcreteBaseCheck baseCheck = new ConcreteBaseCheck();
        makeTest(baseCheck);


        System.out.println("The DiffCheck says...");
        ConcreteDiffCheck diffCheck = new ConcreteDiffCheck();
        diffCheck.specificRequestDetails(number, name);
        diffCheck.specificRequestName();

        System.out.println("\nThe DiffCheckAdapter says...");
        BaseCheck adapter = new DiffCheckAdapter(diffCheck);
        makeTest(adapter);
    }

    static void makeTest(BaseCheck baseCheck) {
        baseCheck.requestDetails(name, number);
        baseCheck.requestName();
    }
}
