package rooms.manager.service.adapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConcreteDiffCheck implements DiffCheck {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConcreteDiffCheck.class);

    @Override
    public void specificRequestDetails(int number, String name) {
        LOGGER.warn("Name Checked SPECIFIC REQUEST" + "\n Name: " + name + "\n Number: " + number);
        System.out.println("Name Checked SPECIFIC REQUEST" + "\n Name: " + name + "\n Number: " + number);


    }

    public void specificRequestName() {
        LOGGER.warn("Details checked with DiffCheck");
        System.out.println("Details checked with DiffCheck");
    }
}
