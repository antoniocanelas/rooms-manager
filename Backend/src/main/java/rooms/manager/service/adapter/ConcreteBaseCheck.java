package rooms.manager.service.adapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConcreteBaseCheck implements BaseCheck {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConcreteBaseCheck.class);


    @Override
    public void requestDetails(String name, int number) {
        LOGGER.warn("Name Checked REQUEST" + "\n Name: " + name + "\n Number: " + number);
        System.out.println("Name Checked REQUEST" + "\n Name: " + name + "\n Number: " + number);
    }

    public void requestName() {
        LOGGER.warn("Details Checked");
        System.out.println("Details Checked");
    }
}
