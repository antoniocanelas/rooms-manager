package rooms.manager.service.adapter;

public class DiffCheckAdapter implements BaseCheck {

    DiffCheck diffCheck;

    public DiffCheckAdapter(DiffCheck diffCheck) {
        this.diffCheck = diffCheck;
    }


    @Override
    public void requestDetails(String name, int number) {
        diffCheck.specificRequestDetails(number, name);
    }

    public void requestName() {
        for (int i = 0; i < 5; i++) {
            diffCheck.specificRequestName();
        }
    }
}
