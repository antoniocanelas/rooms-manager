package rooms.manager.service;

import org.springframework.stereotype.Service;
import rooms.manager.model.RoomComment;

@Service
public interface CommentService {

    void createComment(RoomComment roomComment);

    void appendComment(RoomComment roomComment);
}
