package rooms.manager.service.tax.imp;

import rooms.manager.model.Room;
import rooms.manager.service.tax.Tax;

public class TaxLib implements Tax {

    @Override
    public double calculate(Room room) {

        return room.getMonthRent()* 0.28 ;
    }
}
