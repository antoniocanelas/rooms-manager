package rooms.manager.service.tax;

import rooms.manager.model.Room;

public interface Tax {

    double calculate(Room room);
}
