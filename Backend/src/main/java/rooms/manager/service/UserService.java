package rooms.manager.service;

import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import rooms.manager.model.DTO.UserDTO;
import rooms.manager.model.DTO.UserRegistrationDTO;
import rooms.manager.model.User;

import java.util.List;

public interface UserService {


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    List<UserDTO> findAll();

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void delete(long id);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    User findByUsername(String username);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    UserDTO findById(Long id);

    @PostFilter("filterObject != 'Admin'")
    List<String> listUsernames();

    UserDTO createUserAccount(UserRegistrationDTO accountDto);//TODO Implement available only for unregistered

}
