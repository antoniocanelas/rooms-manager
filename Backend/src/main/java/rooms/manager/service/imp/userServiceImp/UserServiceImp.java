package rooms.manager.service.imp.userServiceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rooms.manager.model.DTO.UserDTO;
import rooms.manager.model.DTO.UserRegistrationDTO;
import rooms.manager.model.User;
import rooms.manager.repository.UserDAO;
import rooms.manager.repository.VerificationTokenRepository;
import rooms.manager.service.UserService;

import java.util.ArrayList;
import java.util.List;

@Service(value = "userService")
public class UserServiceImp implements UserService {

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;
    @Autowired
    private UserDAO userDAO;

    public User save(User user) {
        return userDAO.save(user);
    }

    @Override
    public List<UserDTO> findAll() {
        List<UserDTO> list = new ArrayList<>();
        userDAO.findAll().iterator().forEachRemaining(e -> list.add(e.toDTO()));
        return list;
    }

    @Override
    public void delete(long id) {
        verificationTokenRepository.deleteById(id);
    }

    @Override
    public User findByUsername(String username) {
        return userDAO.findByUsername(username);
    }

    @Override
    public UserDTO findById(Long id) {
        return userDAO.getOne(id).toDTO();
    }

    @Override
    public List<String> listUsernames() {
        List<String> listUsernames = new ArrayList<>();
        userDAO.findAll().iterator().forEachRemaining(e -> listUsernames.add(e.getUsername()));
        return listUsernames;
    }

    @Override
    public UserDTO createUserAccount(UserRegistrationDTO accountDto) {
        return null;
    }

}