package rooms.manager.service.imp.roomServiceImp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import rooms.manager.controller.LoggingController;
import rooms.manager.model.DTO.DocDTO;
import rooms.manager.model.DTO.RoomConverter;
import rooms.manager.model.DTO.RoomDTO;
import rooms.manager.model.User;
import rooms.manager.repository.RoomDAO;
import rooms.manager.service.RoomService;
import rooms.manager.service.methodFactory.*;

import java.util.List;

@Service(value = "docService")
public class DocServiceImp implements RoomService {

    private final static Logger LOGGER = LoggerFactory.getLogger(LoggingController.class);

    @Autowired
    RoomDAO roomDao;
    private RoomConverter roomConverter = new RoomConverter();


    public DocServiceImp(RoomDAO roomDAO) {
        this.roomDao = roomDAO;
    }


    @Override
    public RoomDTO addRoom(RoomDTO roomDTO) {
        return null;
    }

    @Override
    public RoomDTO updateRoom(RoomDTO roomDTO) {
        return null;
    }

    @Override
    public RoomDTO getRoomById(int id) {
        return null;
    }

    @Override
    public void deleteRoomById(int id) {

    }

    @Override
    public void deleteAllRooms() {
    }

    @Override
    public List<RoomDTO> getAllRooms() {
        return null;
    }

    /**
     * Software Design Patterns - Abstract Factory
     */
    @Override
    public DocDTO getDocAbstractFactory(int id, String docTypeSelected) {
        RoomDTO roomDTO = roomConverter.convertFromEntity(this.roomDao.findById(id).get());
        String role;
        DocType docType = DocType.valueOf(docTypeSelected.toUpperCase());


        User.UserRole userRole = getUSerRole();


        DocFactory factory = AbstractDocFactory.getFactory(userRole);
        Doc document = factory.createDoc(docType, roomDTO);



        return new DocDTO(userRole.toString(), document.toString());
    }

    private User.UserRole getUSerRole() {

        if (hasRole("ROLE_ADMIN")) {
            return User.UserRole.ROLE_ADMIN;
        } else if (hasRole("ROLE_GUEST")) {
            return User.UserRole.ROLE_GUEST;
        }
        return null;
    }

    /**
     * Software Design Patterns - Method Factory
     */
    @Override
    public DocDTO getDocFactoryMethod(int id, String docTypeSelected) {
        RoomDTO roomDTO = roomConverter.convertFromEntity(this.roomDao.findById(id).get());
        String role;

        DocFactory docFactory;
        DocType docType = DocType.valueOf(docTypeSelected.toUpperCase());
        Doc doc = null;
        if (hasRole("ROLE_ADMIN")) {
            role = "Admin";
            docFactory = new LandlordDocFactory();
            doc = docFactory.createDoc(docType, roomDTO);

        } else if (hasRole("ROLE_GUEST")) {
            role = "Guest";
            docFactory = new TenantDocFactory();
            doc = docFactory.createDoc(docType, roomDTO);

        } else {
            role = "Without role";
        }

        return new DocDTO(role, doc.toString());
    }

    private boolean hasRole(String role) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        boolean hasRole = authentication.getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals(role));

        return hasRole;
    }


}

