package rooms.manager.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rooms.manager.model.DTO.*;
import rooms.manager.model.Room;
import rooms.manager.model.User;
import rooms.manager.repository.RoomDAO;
import rooms.manager.repository.VerificationTokenRepository;
import rooms.manager.service.RoomService;
import rooms.manager.service.UserService;

import java.util.List;

@Service
public class RoomUserServiceImp implements RoomService, UserService {

    @Autowired
    RoomDAO roomDao;
    @Autowired
    VerificationTokenRepository verificationTokenRepository;
    private RoomConverter roomConverter = new RoomConverter();

    @Override
    public RoomDTO addRoom(RoomDTO roomDTO) {
        return null;
    }

    @Override
    public RoomDTO updateRoom(RoomDTO roomDTO) {
        if (!roomDao.existsById(roomDTO.getRoom_Id())) {
            return null;
        }
        Room room = roomDao.findById(roomDTO.getRoom_Id()).get();
        if (roomDTO.getName() != null) room.setName(roomDTO.getName());
        if (roomDTO.getNumber() != null) room.setNumber(roomDTO.getNumber());
        if (roomDTO.getType() != null) room.setType(roomDTO.getType());
        return roomConverter.convertFromEntity(this.roomDao.save(room));
    }

    @Override
    public RoomDTO getRoomById(int id) {
        return roomConverter.convertFromEntity(this.roomDao.findById(id).get());
    }

    @Override
    public void deleteRoomById(int id) {
        this.roomDao.deleteById(id);
    }

    @Override
    public void deleteAllRooms() {
        this.roomDao.deleteAll();
    }

    @Override
    public List<RoomDTO> getAllRooms() {
        List<Room> roomList = roomDao.findAll();
        return roomConverter.createFromEntities(roomList);
    }

    @Override
    public DocDTO getDocAbstractFactory(int id, String docTypeSelected) {
        return null;
    }

    @Override
    public DocDTO getDocFactoryMethod(int id, String docTypeSelected) {
        return null;
    }


    @Override
    public List<UserDTO> findAll() {
        return null;
    }

    @Override
    public void delete(long id) {

    }

    @Override
    public User findByUsername(String username) {
        return null;
    }

    @Override
    public UserDTO findById(Long id) {
        return null;
    }

    @Override
    public List<String> listUsernames() {
        return null;
    }

    @Override
    public UserDTO createUserAccount(UserRegistrationDTO accountDto) {
        return null;
    }

}
