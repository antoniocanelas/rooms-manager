package rooms.manager.service.imp.roomServiceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rooms.manager.model.DTO.DocDTO;
import rooms.manager.model.DTO.RoomConverter;
import rooms.manager.model.DTO.RoomDTO;
import rooms.manager.model.Room;
import rooms.manager.repository.RoomDAO;
import rooms.manager.service.RoomService;
import rooms.manager.service.command.CommandInvoker;
import rooms.manager.service.command.RoomActivationCommand;

import java.util.List;

@Service
public class RoomServiceImp implements RoomService {
    @Autowired
    RoomDAO roomDao;
    private RoomConverter roomConverter = new RoomConverter();


    public RoomServiceImp(RoomDAO roomDAO) {
        this.roomDao = roomDAO;
    }

    @Override
    public RoomDTO addRoom(RoomDTO roomDTO) {
        Room room = new Room(roomDTO.getName(), roomDTO.getNumber(), roomDTO.getType(),roomDTO.getMonthRent());
        return roomConverter.convertFromEntity(this.roomDao.save(room));
    }

    @Override
    public RoomDTO updateRoom(RoomDTO roomDTO) {
        if (!roomDao.existsById(roomDTO.getRoom_Id())) {
            return null;
        }
        Room room = roomDao.findById(roomDTO.getRoom_Id()).get();
        if (roomDTO.getName() != null) room.setName(roomDTO.getName());
        if (roomDTO.getNumber() != null) room.setNumber(roomDTO.getNumber());
        if (roomDTO.getType() != null) room.setType(roomDTO.getType());
        if (roomDTO.getMonthRent() != 0) room.setMonthRent(roomDTO.getMonthRent());
        return roomConverter.convertFromEntity(this.roomDao.save(room));
    }

    @Override
    public RoomDTO getRoomById(int id) {
        return roomConverter.convertFromEntity(this.roomDao.findById(id).get());
    }

    @Override
    public void deleteRoomById(int id) {
        this.roomDao.deleteById(id);
    }

    @Override
    public void deleteAllRooms() {
        this.roomDao.deleteAll();
    }

    @Override
    public List<RoomDTO> getAllRooms() {
        List<Room> roomList = roomDao.findAll();

        return roomConverter.createFromEntities(roomList);
    }

    @Override
    public DocDTO getDocAbstractFactory(int id, String docTypeSelected) {
        return null;
    }

    @Override
    public DocDTO getDocFactoryMethod(int id, String docTypeSelected) {
        return null;
    }


    public RoomDTO activateRoom(int id) {
        Room room = roomDao.findById(id).get();
        RoomActivationCommand roomActivationCommand = new RoomActivationCommand(room);

        roomActivationCommand.execute();


        return roomConverter.convertFromEntity(this.roomDao.save(room));
    }

    public RoomDTO unActivateRoom(int id) {
        Room room = roomDao.findById(id).get();
        RoomActivationCommand roomActivationCommand = new RoomActivationCommand(room);

        roomActivationCommand.unExecute();


        return roomConverter.convertFromEntity(this.roomDao.save(room));
    }
}
