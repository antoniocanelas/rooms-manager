package rooms.manager.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import rooms.manager.model.Booking;
import rooms.manager.model.DTO.BookingDTO;
import rooms.manager.model.Room;
import rooms.manager.model.User;
import rooms.manager.repository.BookingDAO;
import rooms.manager.repository.RoomDAO;
import rooms.manager.repository.UserDAO;
import rooms.manager.service.BookingFacade;

@Service
public class BookingRoomUserServiceImp {

    @Autowired
    UserDAO userDAO;
    @Autowired
    RoomDAO roomDao;
    @Autowired
    BookingDAO bookingDAO;

    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_GUEST')")
    public BookingDTO addBooking(BookingDTO bookingDTO) {
//        System.out.println(bookingDTO.getUser_Id());
//        System.out.println(bookingDTO.getRoom_Id());
//        System.out.println(bookingDTO.getBooking_Id());
        User user = userDAO.getOne(bookingDTO.getUser_Id());
        Room room = roomDao.getOne(bookingDTO.getRoom_Id());


        BookingFacade bookingFacade = new BookingFacade(user, room);

        if (!bookingFacade.isRoomRegistered()) return null;
        if (!bookingFacade.isUserRegistered()) return null;
        if (!bookingFacade.isUserEnabled()) return null;
        if (!bookingFacade.isRoomActice()) return null;
        if (!bookingFacade.roomsIsFreeInBookingDates()) return null;//TODO define method
        bookingFacade.buildBooking();
        bookingFacade.bookingDefineUser();
        bookingFacade.bookingDefineRoom();
        bookingFacade.bookingSetValid();
        Booking booking = bookingFacade.getBooking();

        return bookingDAO.save(booking).toDTO();
    }

}
