package rooms.manager.service.imp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import rooms.manager.model.DTO.UserRegistrationDTO;
import rooms.manager.model.User;
import rooms.manager.model.VerificationToken;
import rooms.manager.repository.UserDAO;
import rooms.manager.repository.VerificationTokenRepository;
import rooms.manager.service.SecurityService;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

@Service(value = "securityService")
public class SecurityServiceImp implements SecurityService, UserDetailsService {

    public static final String TOKEN_INVALID = "invalidToken";
    public static final String TOKEN_EXPIRED = "expired";
    public static final String TOKEN_VALID = "valid";
    private final static Logger LOGGER = LoggerFactory.getLogger(SecurityServiceImp.class);
    @Resource
    private UserDAO userDao;
    @Autowired
    private VerificationTokenRepository verificationTokenRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        User user = userDao.findByUsername(userId);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        enabled = user.isEnabled();
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, getAuthority());
    }

    @Override
    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }

    @Override
    public User getUserToAuthentication(String username) {
        return userDao.findByUsername(username);
    }

    @Override
    public void createVerificationTokenForUser(User user, String token) {
        final VerificationToken myToken = new VerificationToken(token, user);
        verificationTokenRepository.save(myToken);
    }

    @Override
    public User registerNewUserAccount(UserRegistrationDTO userRegistrationDTO) {
        LOGGER.debug("Registering user account with information: {}", userRegistrationDTO);

        if (userDao.existsUsersByEmail(userRegistrationDTO.getEmail())) {
            LOGGER.warn("There is an account with that email adress: " + userRegistrationDTO.getEmail());
            return null;
        }
        User user = new User();
        user.setUsername(userRegistrationDTO.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(userRegistrationDTO.getPassword()));
        user.setEmail(userRegistrationDTO.getEmail());
        user.setAge(userRegistrationDTO.getAge());
        user.setSalary(userRegistrationDTO.getSalary());
        user.setUserRole(User.UserRole.ROLE_GUEST);

        return userDao.save(user);

    }


    @Override
    public boolean userActivation(String token) { //Rest "/user-activation" call this old
        VerificationToken verificationToken = getVerificationToken(token);
        //Check if token is valid
        if (verificationToken == null) {
            return false;

        }

        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();
        //Check if token is expired
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            return false;
        }

        user.setEnabled(true);
        userDao.save(user);
        return true;
    }

    @Override
    public VerificationToken getVerificationToken(final String VerificationToken) {
        return verificationTokenRepository.findByToken(VerificationToken);
    }

    @Override
    public List<SimpleGrantedAuthority> getAuthority() {
        return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }

}
