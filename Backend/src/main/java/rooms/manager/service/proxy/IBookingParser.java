package rooms.manager.service.proxy;

public interface IBookingParser {
    int getNumberOfLetters();
}