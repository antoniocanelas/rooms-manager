package rooms.manager.service.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rooms.manager.model.DTO.BookingDTO;

/**
 * Software Design Patterns - Proxy Pattern
 */
public class LazyBookingParserProxy implements IBookingParser {
    private final static Logger LOGGER = LoggerFactory.getLogger(LazyBookingParserProxy.class);
    private BookingParser parser = null;
    private BookingDTO bookingDTO = null;

    public LazyBookingParserProxy(BookingDTO bookingDTO) {
        LOGGER.info("LazyBookingParserProxy - Not Expensive Operation");
        this.bookingDTO = bookingDTO;
    }

    @Override
    public int getNumberOfLetters() {
        LOGGER.info("LazyBookingParserProxy.getNumberOfLetters() method - Will call BookingParser if is null");
        if (this.parser == null) {
            this.parser = new BookingParser(bookingDTO);
        }
        return this.parser.getNumberOfLetters();
    }


}
