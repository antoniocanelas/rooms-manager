package rooms.manager.service.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rooms.manager.model.DTO.BookingDTO;

public class BookingParser implements IBookingParser {
    private final static Logger LOGGER = LoggerFactory.getLogger(BookingParser.class);

    public String booking;

    public BookingParser(BookingDTO bookingDTO) {//*** Expensive operation ***
        LOGGER.warn("BookingParser - Expensive Operation");
        this.booking = bookingDTO.toString(); //Simulates a expensive operation, that I want to avoid if possible.
    }

    @Override
    public int getNumberOfLetters() {
        return booking.length();
    }
}
