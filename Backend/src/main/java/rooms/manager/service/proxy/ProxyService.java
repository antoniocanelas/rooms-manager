package rooms.manager.service.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rooms.manager.model.DTO.BookingDTO;

public class ProxyService {
    private final static Logger LOGGER = LoggerFactory.getLogger(ProxyService.class);
    private BookingDTO bookingDTO;

    public ProxyService(BookingDTO bookingDTO) {
        this.bookingDTO = bookingDTO;
    }

    public void proxyTest() {

        LOGGER.debug("Phase 0 - Instantiating BookingParser, no use of proxy");
        new BookingParser(bookingDTO);

        LOGGER.debug("Phase 1 - Instantiating Proxy - LazyBookingParserProxy");
        LazyBookingParserProxy lazyBookParserProxy = new LazyBookingParserProxy(bookingDTO);

        LOGGER.debug("Phase 2 - Calling method getNumberOfLetters() in LazyBookingParserProxy class");
        lazyBookParserProxy.getNumberOfLetters();


    }
}
