package rooms.manager.service.commentServiceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rooms.manager.model.RoomComment;
import rooms.manager.repository.CommentDAO;
import rooms.manager.service.CommentService;


@Service
public class CommentServiceImp implements CommentService {

    @Autowired
    CommentDAO commentDAO;

    public void createComment(RoomComment roomComment) {
        commentDAO.save(roomComment);
    }

    @Override
    public void appendComment(RoomComment roomComment) {
        commentDAO.save(roomComment);
    }


}
