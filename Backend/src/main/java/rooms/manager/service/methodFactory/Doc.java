package rooms.manager.service.methodFactory;

/**
 * Room Document Interface
 */
public interface Doc {
    DocType getDocType();
}
