package rooms.manager.service.methodFactory;

public class TenantDoc implements Doc {
    private DocType docType;
    private String name;
    private String type;
    private double monthRent;


    public TenantDoc(DocType docType, String name, String type, double monthRent) {
        this.docType = docType;
        this.name = name;
        this.type = type;
        this.monthRent = monthRent;
    }

    @Override
    public DocType getDocType() {
        return docType;
    }


    @Override
    public String toString() {
        return "TenantDoc{" +
                "docType=" + docType +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", monthRent=" + monthRent +
                '}';
    }
}
