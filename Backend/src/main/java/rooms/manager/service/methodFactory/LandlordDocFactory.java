package rooms.manager.service.methodFactory;

import rooms.manager.model.DTO.RoomDTO;

/**
 * Concrete subclass for creating new objects.
 */
public class LandlordDocFactory implements DocFactory {
    @Override
    public Doc createDoc(DocType docType, RoomDTO roomDTO) {
        return new LandlordDoc(docType,roomDTO.getRoom_Id(),roomDTO.getName(), roomDTO.getNumber(), roomDTO.getType().toString(),roomDTO.getMonthRent());
    }
}
