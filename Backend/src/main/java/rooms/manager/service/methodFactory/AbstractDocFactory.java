package rooms.manager.service.methodFactory;

import rooms.manager.model.User;


/**
 * Software Design Patterns - Abstract Factory
 */
public class AbstractDocFactory {
    public static DocFactory getFactory(User.UserRole userRole) {
        switch (userRole) {
            case ROLE_ADMIN:
                return new LandlordDocFactory();
            case ROLE_GUEST:
                return new TenantDocFactory();
            default:
                return null;
        }
    }
}
