package rooms.manager.service.methodFactory;

import rooms.manager.model.DTO.RoomDTO;

/**
 *
 * Concrete subclass for creating new objects.
 *
 */
public class TenantDocFactory implements DocFactory {
    @Override
    public Doc createDoc(DocType docType, RoomDTO roomDTO) {
        return new TenantDoc(docType,roomDTO.getName(),roomDTO.getType().toString(),roomDTO.getMonthRent());
    }

}
