package rooms.manager.service.methodFactory;

public class LandlordDoc implements Doc {
    private DocType docType;

    private Integer room_Id;
    private String name;
    private Integer number;
    private String type;
    private double monthRent;


    public LandlordDoc(DocType docType, Integer room_Id, String name, Integer number, String type, double monthRent) {
        this.docType = docType;
        this.room_Id = room_Id;
        this.name = name;
        this.number = number;
        this.type = type;
        this.monthRent = monthRent;
    }

    @Override
    public DocType getDocType() {
        return docType;
    }

    @Override
    public String toString() {
        return "LandlordDoc{" +
                "docType=" + docType +
                ", room_Id=" + room_Id +
                ", name='" + name + '\'' +
                ", number=" + number +
                ", type='" + type + '\'' +
                ", monthRent=" + monthRent +
                '}';
    }
}
