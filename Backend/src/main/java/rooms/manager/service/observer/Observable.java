package rooms.manager.service.observer;

/**
 *
 * Generic observable Interface
 */
public interface Observable {

    void addObserver(Observer observer);

    void removeObserver(Observer observer);

    void notifyObservers();
}
