package rooms.manager.service.observer.HelpMessageServiceImp;

import org.springframework.stereotype.Service;
import rooms.manager.service.HelpMessage;
import rooms.manager.service.HelpMessageService;
import rooms.manager.service.observer.Observable;
import rooms.manager.service.observer.Observer;

import java.util.ArrayList;
import java.util.List;

@Service
public class HelpMessageObservableServiceImp implements HelpMessageService, Observable {

    List<HelpMessage> helpMessageList;
    List<Observer> observerList;



    public HelpMessageObservableServiceImp() {
        helpMessageList = new ArrayList<>();
        observerList = new ArrayList<>();
    }

    @Override
    public void addHelpMessage(HelpMessage helpMessage) {
        helpMessageList.add(helpMessage);
        notifyObservers();
    }


    @Override
    public void addObserver(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        observerList.remove(observer);

    }

    @Override
    public void notifyObservers() {
        observerList.forEach(Observer::update);
    }


    public List<HelpMessage> getHelpMessageList() {
        return helpMessageList;
    }
}
