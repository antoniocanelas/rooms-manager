package rooms.manager.service.observer;

/**
 *
 * Software Design Patterns - Observer
 *
 * Generic observer Interface
 */
public interface Observer {
    void update();
}
