package rooms.manager.service.observer.HelpMessageServiceImp;

import org.slf4j.LoggerFactory;
import rooms.manager.service.HelpMessage;
import rooms.manager.service.observer.Observer;

import java.util.List;


public class HelpMessageShowObserverImp implements Observer {
    private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(HelpMessageShowObserverImp.class);
    HelpMessageObservableServiceImp helpMessageObservableServiceImp;
    String observerDeviceName;


    public HelpMessageShowObserverImp(HelpMessageObservableServiceImp helpMessageObservableServiceImp, String observerDeviceName) {
        this.helpMessageObservableServiceImp = helpMessageObservableServiceImp;
        this.observerDeviceName = observerDeviceName;
    }

    @Override
    public void update() {
        String messagesInfo = "\n";
        List<HelpMessage> helpMessageList = helpMessageObservableServiceImp.getHelpMessageList();
        int i = 0;
        for (HelpMessage helpMessage : helpMessageList
        ) {
            i++;
            messagesInfo = messagesInfo + "Nº " + i + " Name: " + helpMessage.getName() + " - Message: " + helpMessage.getMessage() + "\n";
        }
        LOGGER.warn("HelpMessage Observer - " + observerDeviceName + "display messages: " + messagesInfo);
    }
}
