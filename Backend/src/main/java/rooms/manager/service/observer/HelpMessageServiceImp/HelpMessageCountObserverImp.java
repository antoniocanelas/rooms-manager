package rooms.manager.service.observer.HelpMessageServiceImp;

import org.slf4j.LoggerFactory;
import rooms.manager.service.observer.Observer;


public class HelpMessageCountObserverImp implements Observer {
    private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(HelpMessageCountObserverImp.class);
    HelpMessageObservableServiceImp helpMessageObservableServiceImp;
    String observerDeviceName;


    public HelpMessageCountObserverImp(HelpMessageObservableServiceImp helpMessageObservableServiceImp, String observerDeviceName) {
        this.helpMessageObservableServiceImp = helpMessageObservableServiceImp;
        this.observerDeviceName = observerDeviceName;
    }

    @Override
    public void update() {
        int numberOfHelpMessages= helpMessageObservableServiceImp.getHelpMessageList().size();
        LOGGER.error("HelpMessage Observer - " + observerDeviceName + "display number of messages: " + numberOfHelpMessages);
    }
}
