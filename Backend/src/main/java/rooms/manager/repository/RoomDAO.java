package rooms.manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rooms.manager.model.Room;


@Repository
public interface RoomDAO extends JpaRepository<Room, Integer> {
    boolean existsByName(String name);
}
