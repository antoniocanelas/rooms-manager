package rooms.manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rooms.manager.model.Booking;

import java.util.Optional;

@Repository
public interface BookingDAO extends JpaRepository<Booking, Long> {
    Optional<Booking> findByRoomOrUser(Integer roomId, Long userId);
}
