package rooms.manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rooms.manager.model.RoomComment;

@Repository
public interface CommentDAO extends JpaRepository<RoomComment, Integer> {
}