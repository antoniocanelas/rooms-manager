package rooms.manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rooms.manager.model.User;
import rooms.manager.model.VerificationToken;

@Repository
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {

    //    VerificationToken findByToken(String token);
//
    VerificationToken findByUser(User user);

    VerificationToken findByToken(String token);

//    Stream<VerificationToken> findAllByExpiryDateLessThan(Date now);

//    void deleteByExpiryDateLessThan(Date now);

//    @Modifying
//    @Query("delete from VerificationToken t where t.expiryDate <= ?1")
//    void deleteAllExpiredSince(Date now);
}