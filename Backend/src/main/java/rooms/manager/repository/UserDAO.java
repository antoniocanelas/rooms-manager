package rooms.manager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rooms.manager.model.User;

@Repository
public interface UserDAO extends JpaRepository<User, Long> {
    User findByUsername(String username);

    User findByEmail(String email);

    boolean existsUsersByEmail(String email);
}
