package rooms.manager.model.Cost;

public class BookingCost {

    private CostStrategy strategy;

    public BookingCost(CostStrategy strategy) {
        this.strategy = strategy;
    }

    public double  valueDiscountRate() {
        return strategy.discountRate();
    }

    public void changeStrategy(CostStrategy strategy) {
        this.strategy = strategy;
    }
}
