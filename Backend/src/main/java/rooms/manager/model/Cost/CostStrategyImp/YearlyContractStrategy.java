package rooms.manager.model.Cost.CostStrategyImp;

import rooms.manager.model.Cost.CostStrategy;

public class YearlyContractStrategy implements CostStrategy {

    @Override
    public double discountRate() {
        return 0.8;
    }
}
