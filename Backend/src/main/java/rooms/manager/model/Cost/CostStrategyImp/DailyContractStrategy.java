package rooms.manager.model.Cost.CostStrategyImp;

import rooms.manager.model.Cost.CostStrategy;

public class DailyContractStrategy implements CostStrategy {

    @Override
    public double discountRate() {
        return 1;
    }
}
