package rooms.manager.model.Cost;

/**
 * Software Design Patterns - Strategy
 */
public interface CostStrategy {
    double discountRate();
}
