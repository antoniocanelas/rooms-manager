package rooms.manager.model.event;


import org.springframework.context.ApplicationEvent;

import java.util.EventObject;

/**
 * This is the base class for domain events. All events must extend this class.
 *
 */
public abstract class CommentsDomainEvent extends ApplicationEvent {

    /** use serialVersionUID from Spring 1.2 for interoperability. */
    private static final long serialVersionUID = 7099057708183571937L;

    /** System time when the event happened. */
    private final long timestamp;


    /**
     * Create a new ApplicationEvent.
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public CommentsDomainEvent(Object source) {
        super(source);
        this.timestamp = System.currentTimeMillis();
    }

    public abstract void process();
}
