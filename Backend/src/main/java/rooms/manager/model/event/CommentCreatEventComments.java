package rooms.manager.model.event;

import rooms.manager.model.DTO.AppendToRoomCommentBookDTO;
import rooms.manager.model.DTO.CreateRoomCommentBookDTO;
import rooms.manager.model.RoomComment;
import rooms.manager.model.event.state.CommentAggregate;

public class CommentCreatEventComments extends CommentsDomainEvent {
    CreateRoomCommentBookDTO createRoomCommentBookDTO;
    long createdTime;



    public CommentCreatEventComments(CreateRoomCommentBookDTO createRoomCommentBookDTO) {
        super(createRoomCommentBookDTO);
        this.createRoomCommentBookDTO = createRoomCommentBookDTO;
        this.createdTime = super.getTimestamp();
    }

    @Override
    public void process() {
        if (CommentAggregate.isCreated(createRoomCommentBookDTO.getRoomNumber())) {
            throw new RuntimeException("Comments already open");
        }

        RoomComment roomComment = new RoomComment(createRoomCommentBookDTO.getRoomNumber(), createRoomCommentBookDTO.getComment());
        roomComment.handleEvent(this);
    }
}