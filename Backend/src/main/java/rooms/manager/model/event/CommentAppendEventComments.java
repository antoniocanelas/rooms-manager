package rooms.manager.model.event;

import rooms.manager.model.DTO.AppendToRoomCommentBookDTO;
import rooms.manager.model.RoomComment;
import rooms.manager.model.event.state.CommentAggregate;

public class CommentAppendEventComments extends CommentsDomainEvent {
    final AppendToRoomCommentBookDTO appendToRoomCommentBookDTO;
    final long createdTime;


    public CommentAppendEventComments(AppendToRoomCommentBookDTO appendToRoomCommentBookDTO) {
        super(appendToRoomCommentBookDTO);
        this.appendToRoomCommentBookDTO=appendToRoomCommentBookDTO;
        this.createdTime = super.getTimestamp();
    }

    @Override
    public void process() {
        if (CommentAggregate.isCreated(appendToRoomCommentBookDTO.getRoomNumber())) {
        RoomComment roomComment = new RoomComment(appendToRoomCommentBookDTO.getRoomNumber(), appendToRoomCommentBookDTO.getComment());
        roomComment.handleEvent(this);
        } else{
            throw new RuntimeException("Comments not open");}
                }
}