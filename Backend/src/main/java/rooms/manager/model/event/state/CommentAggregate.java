/**
 * The MIT License
 * Copyright (c) 2014 Ilkka Seppälä
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE TaxIS PROVIDED "AS TaxIS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package rooms.manager.model.event.state;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rooms.manager.model.DTO.RoomCommentDTO;
import rooms.manager.model.RoomComment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is the static accounts map holder class.
 * This class holds the state of the Room Comments.
 * <p>
 * Created by Serdar Hamzaogullari on 06.08.2017.
 */
public class CommentAggregate {

    private final static Logger LOGGER = LoggerFactory.getLogger(CommentAggregate.class);
    private static Map<Integer, List<RoomCommentDTO>> historyMap = new HashMap<>();

    private CommentAggregate() {
    }

    public static boolean isCreated(int roomNumber) {
        return historyMap.containsKey(roomNumber);
    }

    public static void createCommentBook(RoomComment roomComment) {
        List<RoomCommentDTO> roomCommentDTOList = new ArrayList<>();
        RoomCommentDTO roomCommentDTO = new RoomCommentDTO(roomComment);
        roomCommentDTOList.add(roomCommentDTO);
        historyMap.put(roomComment.getRoomNumber(), roomCommentDTOList);

        LOGGER.warn("Comments Book Created, number of comments " + historyMap.size());
    }

    public static void appendToCommentBook(RoomComment roomComment) {
        RoomCommentDTO roomCommentDTO = new RoomCommentDTO(roomComment);
        historyMap.get(roomComment.getRoomNumber()).add(roomCommentDTO);

        LOGGER.warn("Comments Book with " + historyMap.get(roomComment.getRoomNumber()).size() + " number of comments");
    }
}


