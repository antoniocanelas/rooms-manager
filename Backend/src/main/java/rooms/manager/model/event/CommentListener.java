package rooms.manager.model.event;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class CommentListener implements ApplicationListener<CommentsDomainEvent> {
    @Override
    public void onApplicationEvent(CommentsDomainEvent event) {
        event.process();
    }
}
