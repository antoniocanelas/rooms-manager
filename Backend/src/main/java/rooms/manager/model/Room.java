package rooms.manager.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ROOM_TABLE")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int room_Id;/*Choose .IDENTITY, because with .AUTOMATIC, when add entities in tests with data.sql and test add, same numbers used */
    @Column(name = "ROOM_NAME")
    private String name;
    @Column(name = "ROOM_ACTIVE")
    private boolean isActive;
    @Column(name = "ROOM_NUMBER", length = 10)
    private int number;
    @Column(name = "ROOM_MONTH_RENT", length = 10)
    private double monthRent;
    @Column(name = "ROOM_TYPE", length = 10)
    @Enumerated(EnumType.STRING)
    private RoomType type;
    @OneToMany
    private List<Booking> bookingList;

    public Room(int room_Id, String name, int number, RoomType type, double monthRent) {
        this.room_Id = room_Id;
        this.name = name;
        this.number = number;
        this.type = type;
        this.monthRent = monthRent;
        this.isActive = false;
    }

    public Room(String name, Integer number, RoomType type, double monthRent) {
        this.name = name;
        this.number = number;
        this.type = type;
        this.monthRent = monthRent;
        this.isActive = false;
    }

    public Room() {
    }

    public Room(Integer room_id, String name, Integer number, RoomType type, double monthRent, boolean active) {
        this.room_Id = room_id;
        this.name = name;
        this.number = number;
        this.type = type;
        this.monthRent = monthRent;
        this.isActive = active;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public double getMonthRent() {
        return monthRent;
    }

    public void setMonthRent(double monthRent) {
        this.monthRent = monthRent;
    }

    public List<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Room{");
        sb.append("room_Id=").append(room_Id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", isActive=").append(isActive);
        sb.append(", number=").append(number);
        sb.append(", monthRent=").append(monthRent);
        sb.append(", type=").append(type);
        sb.append(", bookingList=").append(bookingList);
        sb.append('}');
        return sb.toString();
    }

    public Integer getRoom_Id() {
        return room_Id;
    }

    public void setRoom_Id(int room_Id) {
        this.room_Id = room_Id;
    }

    public void setRoom_Id(Integer room_Id) {
        this.room_Id = room_Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoomType getType() {
        return type;
    }

    public void setType(RoomType type) {
        this.type = type;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void inactivateRoom() {
        setActive(false);
    }

    public void activateRoom() {
        setActive(true);
    }

    public enum RoomType {
        SINGLE, DOUBLE
    }

}