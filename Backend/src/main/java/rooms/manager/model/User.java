package rooms.manager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import rooms.manager.model.DTO.UserDTO;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    /* Name ends in "_Id" otherwise JPA would create another column in DB with name "user_Id". Choose .IDENTITY, because with .AUTOMATIC, when add entities in tests with data.sql and test add, same numbers used */
    private long user_Id;
    @Column
    private String username;
    @Column(nullable = false)
    @Email
    private String email;
    @Column
    @JsonIgnore
    private String password;
    @Column
    private long salary;
    @Column
    private int age;
    @Column
    private Boolean enabled;
    @Column(name = "user_role", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private UserRole userRole;
    @OneToMany
    private List<Booking> bookingList;

    public User() {
        super();
        this.enabled = false;
    }

    public List<Booking> getBookingList() {
        return bookingList;
    }

    public void setBookingList(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }

    public UserDTO toDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername(username);
        userDTO.setSalary(salary);
        userDTO.setAge(age);
        userDTO.setUserRole(userRole);
        userDTO.setEmail(email);
        userDTO.setId(user_Id);
        userDTO.setEnabled(enabled);

        return userDTO;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public long getUser_Id() {
        return user_Id;
    }

    public void setUser_Id(long user_Id) {
        this.user_Id = user_Id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("user_Id=").append(user_Id);
        sb.append(", username='").append(username).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", salary=").append(salary);
        sb.append(", age=").append(age);
        sb.append(", enabled=").append(enabled);
        sb.append(", userRole=").append(userRole);
        sb.append(", bookingList=").append(bookingList);
        sb.append('}');
        return sb.toString();
    }

    public enum UserRole {
        ROLE_ADMIN, ROLE_GUEST
    }
}
