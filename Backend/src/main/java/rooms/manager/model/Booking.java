package rooms.manager.model;

import rooms.manager.model.Cost.CostStrategy;
import rooms.manager.model.DTO.BookingDTO;

import javax.persistence.*;

@Entity
@Table(name = "BOOKING_TABLE")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long booking_Id;/*Choose .IDENTITY, because with .AUTOMATIC, when add entities in tests with data.sql and test add, same numbers used */
    @Column
    private boolean isValid;
    @ManyToOne
    private User user;
    @ManyToOne
    private Room room;
    @Transient
    private CostStrategy costStrategy;

    public Booking(CostStrategy costStrategy) {
        this.costStrategy = costStrategy;
    }

    public Booking() {
    }

    public BookingDTO toDTO() {
        BookingDTO bookingDTO = new BookingDTO(booking_Id, room.getRoom_Id(), user.getUser_Id(), isValid);
        return bookingDTO;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CostStrategy getCostStrategy() {
        return costStrategy;
    }

    public void setCostStrategy(CostStrategy costStrategy) {
        this.costStrategy = costStrategy;
    }

    public long getBooking_Id() {
        return booking_Id;
    }

    public void setBooking_Id(long booking_Id) {
        this.booking_Id = booking_Id;
    }
}
