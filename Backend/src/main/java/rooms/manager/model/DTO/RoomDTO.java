package rooms.manager.model.DTO;

import rooms.manager.model.Room;

import java.util.Objects;

public class    RoomDTO {
    private Integer room_Id;
    private String name;
    private Integer number;
    private Room.RoomType type;
    private double monthRent;
    private boolean isActive;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public RoomDTO() {
    }

    public RoomDTO(String name, Integer number, Room.RoomType type, double monthRent) {
        this.name = name;
        this.number = number;
        this.type = type;
        this.monthRent = monthRent;
    }

    public RoomDTO(Integer room_id, String name, Integer number, Room.RoomType type, double monthRent, boolean active) {
        this.room_Id = room_id;
        this.name = name;
        this.number = number;
        this.type = type;
        this.monthRent = monthRent;
        this.isActive = active;
    }

    public double getMonthRent() {
        return monthRent;
    }

    public void setMonthRent(double monthRent) {
        this.monthRent = monthRent;
    }

    public RoomDTO(Integer room_Id, String name, Integer number, Room.RoomType type, double monthRent) {
        this.room_Id = room_Id;
        this.name = name;
        this.number = number;
        this.type = type;
        this.monthRent = monthRent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoomDTO roomDTO = (RoomDTO) o;

        if (Double.compare(roomDTO.monthRent, monthRent) != 0) return false;
        if (room_Id != null ? !room_Id.equals(roomDTO.room_Id) : roomDTO.room_Id != null) return false;
        if (name != null ? !name.equals(roomDTO.name) : roomDTO.name != null) return false;
        if (number != null ? !number.equals(roomDTO.number) : roomDTO.number != null) return false;
        return type == roomDTO.type;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = room_Id != null ? room_Id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        temp = Double.doubleToLongBits(monthRent);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Room.RoomType getType() {
        return type;
    }

    public void setType(Room.RoomType type) {
        this.type = type;
    }

    public Integer getRoom_Id() {
        return room_Id;
    }

    public void setRoom_Id(Integer room_Id) {
        this.room_Id = room_Id;
    }
}
