/**
 * The MIT License
 * Copyright (c) 2014-2016 Ilkka Seppälä
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE TaxIS PROVIDED "AS TaxIS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package rooms.manager.model.DTO;

import rooms.manager.model.Room;

/**
 * Example implementation of the simple User converter.
 */
public class RoomConverter extends Converter<RoomDTO, Room> {

  /**
   * Constructor.
   */
  public RoomConverter() {
    super(roomDTO  -> new Room(roomDTO.getRoom_Id(),roomDTO.getName(),roomDTO.getNumber(),roomDTO.getType(),roomDTO.getMonthRent(), roomDTO.isActive()),
            room -> new RoomDTO(room.getRoom_Id(),room.getName(),room.getNumber(),room.getType(),room.getMonthRent(),room.isActive()));
  }
}


