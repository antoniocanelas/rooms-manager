package rooms.manager.model.DTO;

public class BookingDTO {
    private long booking_Id;
    private Integer room_Id;
    private long user_Id;
    private boolean isValid;


    public BookingDTO(long booking_Id, Integer room_Id, long user_Id, boolean isValid) {
        this.booking_Id = booking_Id;
        this.room_Id = room_Id;
        this.user_Id = user_Id;
        this.isValid = isValid;
    }

    public BookingDTO() {
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public long getBooking_Id() {
        return booking_Id;
    }

    public void setBooking_Id(long booking_Id) {
        this.booking_Id = booking_Id;
    }

    public Integer getRoom_Id() {
        return room_Id;
    }

    public void setRoom_Id(Integer room_Id) {
        this.room_Id = room_Id;
    }

    public long getUser_Id() {
        return user_Id;
    }

    public void setUser_Id(long user_Id) {
        this.user_Id = user_Id;
    }
}
