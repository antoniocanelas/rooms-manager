package rooms.manager.model.DTO;

public class DocDTO {
    String role;
    String info;


    public DocDTO(String role,String info) {
        this.role = role;
        this.info = info;
    }

    @Override
    public String toString() {
        return "DocDTO{" +
                "role='" + role + '\'' +
                ", info='" + info + '\'' +
                '}';
    }
}
