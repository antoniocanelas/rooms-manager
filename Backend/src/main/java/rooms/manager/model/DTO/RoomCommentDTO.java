package rooms.manager.model.DTO;

import rooms.manager.model.RoomComment;

public class RoomCommentDTO {

    String comment;
    int roomNumber;

    public RoomCommentDTO() {
    }

    public RoomCommentDTO(RoomComment roomComment) {
        comment = roomComment.getComment();
        roomNumber = roomComment.getRoomNumber();

    }
}
