package rooms.manager.model.DTO;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * Software Design Patterns - Converter
 *
 * Generic converter, thanks to Java8 features not only provides a way of generic bidirectional
 * conversion between corresponding types, but also a common way of converting a collection of objects
 * of the same type, reducing boilerplate code to the absolute minimum.
 * @param <T> DTO representation's type
 * @param <U> Domain representation's type
 */
public class Converter<T, U> {

  private final Function<T, U> fromDTO;
  private final Function<U, T> fromEntity;

  /**
   * @param fromDTO Function that converts given dto entity into the domain entity.
   * @param fromEntity Function that converts given domain entity into the dto entity.
   */
  public Converter(final Function<T, U> fromDTO, final Function<U, T> fromEntity) {
    this.fromDTO = fromDTO;
    this.fromEntity = fromEntity;
  }

  /**
   * @param dTO DTO entity
   * @return The domain representation - the result of the converting function application on DTO entity.
   */
  public final U convertFromDto(final T dTO) {
    return fromDTO.apply(dTO);
  }

  /**
   * @param entity domain entity
   * @return The DTO representation - the result of the converting function application on domain entity.
   */
  public final T convertFromEntity(final U entity) {
    return fromEntity.apply(entity);
  }

  /**
   * @param dTO collection of DTO entities
   * @return List of domain representation of provided entities retrieved by
   *        mapping each of them with the conversion function
   */
  public final List<U> createFromDtos(final Collection<T> dTO) {
    return dTO.stream().map(this::convertFromDto).collect(Collectors.toList());
  }

  /**
   * @param entities collection of domain entities
   * @return List of domain representation of provided entities retrieved by
   *        mapping each of them with the conversion function
   */
  public final List<T> createFromEntities(final Collection<U> entities) {
    return entities.stream().map(this::convertFromEntity).collect(Collectors.toList());
  }

}
