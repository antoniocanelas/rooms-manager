package rooms.manager.model.DTO;

import rooms.manager.model.User;

public class AppendToRoomCommentBookDTO {

    private String comment;
    private int roomNumber;

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
