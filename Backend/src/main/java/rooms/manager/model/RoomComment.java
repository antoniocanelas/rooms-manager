package rooms.manager.model;

import rooms.manager.model.DTO.AppendToRoomCommentBookDTO;
import rooms.manager.model.DTO.CreateRoomCommentBookDTO;
import rooms.manager.model.event.CommentAppendEventComments;
import rooms.manager.model.event.CommentCreatEventComments;
import rooms.manager.model.event.state.CommentAggregate;

import javax.persistence.*;

@Entity
@Table(name = "COMMENT_TABLE")
public class RoomComment {
    @Column(name = "ROOM_NUMBER", length = 10)
    private final int roomNumber;
    @Column(name = "COMMENT")
    private final String comment;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int roomComment_Id;



    public RoomComment(CreateRoomCommentBookDTO createRoomCommentBookDTO) {
        this.roomNumber = createRoomCommentBookDTO.getRoomNumber();
        this.comment = "Created comments booking for room " + roomNumber;
    }

    public RoomComment(AppendToRoomCommentBookDTO appendToRoomCommentBookDTO) {
        this.roomNumber = appendToRoomCommentBookDTO.getRoomNumber();
        this.comment = appendToRoomCommentBookDTO.getComment();
    }

    public RoomComment(int roomNumber, String comment) {
        this.roomNumber = roomNumber;
        this.comment = comment;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public String getComment() {
        return comment;
    }


    public int getRoomComment_Id() {
        return roomComment_Id;
    }

    public void setRoomComment_Id(int roomComment_Id) {
        this.roomComment_Id = roomComment_Id;
    }

    public void handleEvent(CommentCreatEventComments commentCreatEvent) {
        CommentAggregate.createCommentBook(this);
    }

    public void handleEvent(CommentAppendEventComments commentAppendEventComments) {
        CommentAggregate.appendToCommentBook(this);
    }
}
