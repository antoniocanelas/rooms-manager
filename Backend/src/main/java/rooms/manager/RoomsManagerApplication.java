package rooms.manager;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication/*(exclude = { SecurityAutoConfiguration.class })*/
public class RoomsManagerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(RoomsManagerApplication.class, args);
    }

    @Override
    public void run(String... args) {
    }

}
