package rooms.manager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import rooms.manager.model.DTO.AppendToRoomCommentBookDTO;
import rooms.manager.model.DTO.CreateRoomCommentBookDTO;
import rooms.manager.model.RoomComment;
import rooms.manager.model.event.CommentAppendEventComments;
import rooms.manager.model.event.CommentCreatEventComments;
import rooms.manager.service.commentServiceImp.CommentServiceImp;

@RestController
@RequestMapping
public class CommentController {

    @Autowired
    CommentServiceImp commentServiceImp;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @PostMapping(value = "roomcomment/{roomNumber}")
    @ResponseStatus(HttpStatus.CREATED)
    public void createRoomCommentBook(@PathVariable int roomNumber, @RequestBody CreateRoomCommentBookDTO createRoomCommentBookDTO) {
        createRoomCommentBookDTO.setRoomNumber(roomNumber);
        RoomComment roomComment = new RoomComment(createRoomCommentBookDTO);

        commentServiceImp.createComment(roomComment); /* Save in DB */
        CommentCreatEventComments commentCreatEvent = new CommentCreatEventComments(createRoomCommentBookDTO);
        eventPublisher.publishEvent(commentCreatEvent); /* Order to change state */
    }

    @PutMapping(value = "roomcomment/{roomNumber}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void appendToRoomCommentBook(@PathVariable int roomNumber, @RequestBody AppendToRoomCommentBookDTO appendToRoomCommentBookDTO) {
        appendToRoomCommentBookDTO.setRoomNumber(roomNumber);
        RoomComment roomComment = new RoomComment(appendToRoomCommentBookDTO);
        commentServiceImp.appendComment(roomComment); /* Save in DB */
        CommentAppendEventComments commentAppendEventComments = new CommentAppendEventComments(appendToRoomCommentBookDTO);
        eventPublisher.publishEvent(commentAppendEventComments); /* Order to change state */
    }
}
