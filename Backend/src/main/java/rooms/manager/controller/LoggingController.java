package rooms.manager.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@SuppressWarnings("SameReturnValue")
@RestController
public class LoggingController {

    private final static Logger LOGGER = LoggerFactory.getLogger(LoggingController.class);

    @GetMapping("/logger")
    public String index() {
        LOGGER.trace("A TRACE Message");
        LOGGER.debug("A DEBUG Message");
        LOGGER.info("An INFO Message");
        LOGGER.warn("A WARN Message");
        LOGGER.error("An ERROR Message");

        for (int i = 0; i < 10; i++) {
            LOGGER.info("This is the {} time I say 'Logger Test'.", i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
                LOGGER.error("Thread.sleep", e.getMessage());
            }
        }

        return "Check out the Logs to see the output... :-) - Commit 106";
    }

    @GetMapping("/revision")
    public String reision() {

        String rev = "";
        try {
            rev = new String(Files.readAllBytes(Paths.get("./revision.txt")));
        } catch (IOException e) {
            e.printStackTrace();
        }


        return rev;
    }
}