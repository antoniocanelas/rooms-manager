package rooms.manager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import rooms.manager.model.DTO.BookingDTO;
import rooms.manager.service.imp.BookingRoomUserServiceImp;

@RestController
@RequestMapping("/booking")
public class BookingController {

    @Autowired
    BookingRoomUserServiceImp bookingRoomUserServiceImp;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BookingDTO addBooking(@RequestBody BookingDTO bookingDTO) {
        return this.bookingRoomUserServiceImp.addBooking(bookingDTO);
    }


}
