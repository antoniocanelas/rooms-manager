package rooms.manager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rooms.manager.model.DTO.RoomDTO;
import rooms.manager.service.imp.roomServiceImp.RoomServiceImp;

@RestController
@RequestMapping("/command")
public class CommandController {
    @Autowired
    RoomServiceImp roomServiceImp;

    public CommandController(RoomServiceImp roomServiceImp) {
        this.roomServiceImp = roomServiceImp;
    }

    @PutMapping(value = "{id}/activateRoom")
    public RoomDTO activateRoom(@PathVariable int id) {
        return this.roomServiceImp.activateRoom(id);
    }

    @PutMapping(value = "/{id}/un-activateRoom")
    public RoomDTO unActivateRoom(@PathVariable int id) {
        return this.roomServiceImp.unActivateRoom(id);
    }

}
