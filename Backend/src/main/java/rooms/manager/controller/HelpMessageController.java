package rooms.manager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import rooms.manager.service.HelpMessage;
import rooms.manager.service.HelpMessageService;
import rooms.manager.service.observer.HelpMessageServiceImp.HelpMessageCountObserverImp;
import rooms.manager.service.observer.HelpMessageServiceImp.HelpMessageObservableServiceImp;
import rooms.manager.service.observer.HelpMessageServiceImp.HelpMessageShowObserverImp;

@RestController
@RequestMapping
public class HelpMessageController {

    @Autowired
    HelpMessageService helpMessageService;
    @Autowired
    HelpMessageObservableServiceImp helpMessageObservableServiceImp;

    @PostMapping(value = "help-message")
    @ResponseStatus(HttpStatus.CREATED)
    public void addComment(@RequestBody HelpMessage helpMessage) {

        helpMessageService.addHelpMessage(helpMessage);
    }

    @PostMapping(value = "help-message/add-count-observer")
    @ResponseStatus(HttpStatus.CREATED)
    public void addCountObserver(@RequestBody String observerDeviceName) {
        helpMessageObservableServiceImp.addObserver(new HelpMessageCountObserverImp(helpMessageObservableServiceImp, observerDeviceName));

    }

    @PostMapping(value = "help-message/add-show-observer")
    @ResponseStatus(HttpStatus.CREATED)
    public void addShowMessageObserver(@RequestBody String observerDeviceName) {
        helpMessageObservableServiceImp.addObserver(new HelpMessageShowObserverImp(helpMessageObservableServiceImp, observerDeviceName));

    }

}

