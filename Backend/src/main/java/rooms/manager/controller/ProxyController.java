package rooms.manager.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import rooms.manager.model.DTO.BookingDTO;
import rooms.manager.service.proxy.ProxyService;

@RestController
@RequestMapping
public class ProxyController {

    @PostMapping(value = "/proxy")
    @ResponseStatus(HttpStatus.OK)
    public void createRoomCommentBook(@RequestBody BookingDTO bookingDTO) {
        ProxyService proxyService = new ProxyService(bookingDTO);
        proxyService.proxyTest();

    }


}
