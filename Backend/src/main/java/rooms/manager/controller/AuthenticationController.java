package rooms.manager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rooms.manager.config.JwtTokenUtil;
import rooms.manager.model.AuthToken;
import rooms.manager.model.DTO.LoginUserDTO;
import rooms.manager.model.DTO.UserRegistrationDTO;
import rooms.manager.model.User;
import rooms.manager.registration.OnRegistrationCompleteEvent;
import rooms.manager.service.SecurityService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/token")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Qualifier("securityService")
    @Autowired
    private SecurityService securityService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @PostMapping(value = "/generate-token")
    public ResponseEntity<?> login(@RequestBody LoginUserDTO loginUserDTO) throws AuthenticationException {

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUserDTO.getUsername(),
                        loginUserDTO.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final User user = securityService.findByUsername(loginUserDTO.getUsername());
        final String token = jwtTokenUtil.generateToken(user);
        return ResponseEntity.ok(new AuthToken(token));
    }

    @SuppressWarnings("unchecked")
    @PostMapping(value = "/registration")
    public ResponseEntity<?> registerUserAccount(@Valid @RequestBody UserRegistrationDTO userRegistrationDTO, BindingResult result, final HttpServletRequest request) {

        if (result.hasErrors()) {
            //noinspection unchecked
            return new ResponseEntity("Wrong Parameters", HttpStatus.BAD_REQUEST);
        }
        final User registered = securityService.registerNewUserAccount(userRegistrationDTO);
        String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        OnRegistrationCompleteEvent onRegistrationCompleteEvent = new OnRegistrationCompleteEvent(registered, request.getLocale(), appUrl);
        eventPublisher.publishEvent(onRegistrationCompleteEvent);
        if (registered == null) {
            //noinspection unchecked
            return new ResponseEntity("Email already registered", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(registered, HttpStatus.CREATED);
    }

    @GetMapping(value = "/user-activation")
    public ResponseEntity<?> userActivation(@RequestParam("token") String token, final HttpServletRequest request) {
        boolean isUserActivated=securityService.userActivation(token);
        if (!isUserActivated) {
            //noinspection unchecked
            return new ResponseEntity("Invalid Token / Expired Token", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
