package rooms.manager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import rooms.manager.model.DTO.UserDTO;
import rooms.manager.service.UserService;

import java.util.List;

@RestController
public class UserController {

    @Qualifier("userService")
    @Autowired
    private UserService userService;

    @GetMapping(value = "/user")
    public List<UserDTO> listUser() {
        return userService.findAll();
    }

    @GetMapping(value = "/user/{id}")
    public UserDTO getOne(@PathVariable(value = "id") Long id) {
        return userService.findById(id);
    }


    @GetMapping(value = "/user/other-users")
    public List<String> getAllUsernamesExceptCurrent() {
        return userService.listUsernames();
    }


}
