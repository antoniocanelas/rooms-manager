package rooms.manager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import rooms.manager.model.DTO.RoomDTO;
import rooms.manager.service.imp.roomServiceImp.RoomServiceImp;

import java.util.List;

@RestController
@RequestMapping("/room")
public class RoomController {
    @Autowired
    RoomServiceImp roomServiceImp;

    public RoomController(RoomServiceImp roomServiceImp) {
        this.roomServiceImp = roomServiceImp;
    }

    @PostMapping(value = "/addroom")
    @ResponseStatus(HttpStatus.CREATED)
    public RoomDTO addroom(@RequestBody RoomDTO roomDTO) {
        return this.roomServiceImp.addRoom(roomDTO);
    }

    @PutMapping(value = "/updateroom")
    public RoomDTO updateroom(@RequestBody RoomDTO roomDTO) {
        return this.roomServiceImp.updateRoom(roomDTO);
    }

    @GetMapping(value = "/{id}")
    public RoomDTO getroom(@PathVariable int id) {
        return this.roomServiceImp.getRoomById(id);
    }

    @DeleteMapping(value = "/all")
    public void deleteAllrooms() {
        this.roomServiceImp.deleteAllRooms();
    }

    @DeleteMapping(value = "/{id}")
    public void deleteroom(@PathVariable int id) {
        this.roomServiceImp.deleteRoomById(id);
    }

    @GetMapping(value = "/all")
    public List<RoomDTO> getAllRooms() {
        return this.roomServiceImp.getAllRooms();
    }

}
