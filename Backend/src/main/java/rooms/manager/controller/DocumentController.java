package rooms.manager.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rooms.manager.service.RoomService;

@RestController
@RequestMapping("/doc")
public class DocumentController {

    @Qualifier("docService")
    @Autowired
    RoomService roomServiceImp;


@GetMapping(value = "/{id}/abstract-factory/{docTypeSelected}")
    public String getDocAbstractFactory(@PathVariable int id,@PathVariable  String docTypeSelected) {

        return this.roomServiceImp.getDocAbstractFactory(id,docTypeSelected).toString();
    }

    @GetMapping(value = "/{id}/method-factory/{docTypeSelected}")
    public String getDocFactoryMethod(@PathVariable int id,@PathVariable  String docTypeSelected) {

        return this.roomServiceImp.getDocFactoryMethod(id,docTypeSelected).toString();
    }

}
