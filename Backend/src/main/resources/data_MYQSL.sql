INSERT INTO user (email, username, password, salary, age, user_role, enabled)
VALUES ('admin@test.tt',
        'Admin',
        '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu',
        4234,
        45,
        'ROLE_ADMIN',
        true);

INSERT INTO user (email, username, password, salary, age, user_role, enabled)
VALUES ('guest@test.tt',
        'Guest',
        '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu',
        4234,
        45,
        'ROLE_GUEST',
        true);


INSERT INTO user (email, username, password, salary, age, user_role, enabled)
VALUES ('landlord@test.tt',
        'Landlord',
        '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu',
        7823,
        23,
        'ROLE_ADMIN',
        true);

INSERT INTO user (email, username, password, salary, age, user_role, enabled)
VALUES ('tenant@test.tt',
        'Tenant',
        '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu',
        7823,
        23,
        'ROLE_GUEST',
        true);

INSERT INTO room_table(room_Id, room_name, room_number, room_type, room_month_rent, room_active)
VALUES (1, 'getRoom', 1, 'SINGLE', 280.0, true);

INSERT INTO room_table (room_Id, room_name, room_number, room_type, room_month_rent, room_active)
VALUES (2, 'getAllRooms', 1, 'SINGLE', 300.0, true);



INSERT INTO booking_table (room_room_id, user_user_id)
VALUES (1, 1);

